﻿namespace Pinterest.Cookie
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;

    sealed class Http
    {
        public ProxyModel _proxyModel { get; set; }
        public Http(ProxyModel _proxy)
        {
            _proxyModel = _proxy;
        }

        public RequestResult Request(string url, List<string> http_headers, string postBody
            , CookieModel cookieModel)
        {
            ServicePointManager.Expect100Continue = false;
            RequestResult result = new RequestResult() {
                Message = "",
                Result = "",
                IsSucceed = true
            };

             var currentUrlFromSite = System.Web.HttpContext.Current == null
                ? ""
                : System.Web.HttpContext.Current.Request.Url.AbsolutePath;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                foreach (var str in http_headers)
                    request.Headers.Add(str);

                request.UserAgent = cookieModel.UserAgent;


                request.PreAuthenticate = true;
                request.Headers.Add("Authorization", cookieModel.Authorization);
                request.Accept = "application/json";

                if (_proxyModel != null)
                {
                    var wp = new WebProxy(_proxyModel.ip, _proxyModel.port);
                    wp.Credentials = new NetworkCredential(_proxyModel.username, _proxyModel.password);

                    request.Proxy = wp;
                }

                Uri target = new Uri("https://pinterest.com");

                CookieContainer container = new CookieContainer(6);
                //container.Add(new Cookie("Authorization", cookieModel.Authorization) { Domain = target.Host });

                request.CookieContainer = container;

                if (!string.IsNullOrEmpty(postBody))
                {
                    request.Method = "POST";

                    using (var stream = request.GetRequestStream())
                    {
                        var bytes = ASCIIEncoding.ASCII.GetBytes(postBody);
                        stream.Write(bytes, 0, bytes.Length);
                    }
                }

                WebResponse response = request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());

                result.Result = reader.ReadToEnd();
                result.StatusCode = 200;
            }
            catch (WebException exc)
            {
                result.IsSucceed = false;
                result.Message = exc.Message;

                int statusCode = 500;
                var response = exc.Response as HttpWebResponse;
                if (response != null) {
                    statusCode = (int)response.StatusCode;
                    result.StatusCode = statusCode;
                }

                if (exc.Response != null) {
                    var stream = exc.Response.GetResponseStream();
                    if (stream != null) {
                        var resp = new StreamReader(stream).ReadToEnd();
                    }
                }
            }
            catch (Exception exc)
            {
                result.IsSucceed = false;
                result.Message = exc.Message;
                result.StatusCode = 500;
            }

            return result;
        }

        public RequestStreamResult RequestStream(string url, List<string> http_headers, string postBody, CookieModel cookieModel,
            string logField)
        {
            ServicePointManager.Expect100Continue = false;
            var responseStream = new RequestStreamResult()
            {
                IsSucceed = true,
                Message = "",
                Result = null
            };

            var currentUrlFromSite = System.Web.HttpContext.Current.Request.Url.AbsolutePath;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                foreach (var str in http_headers)
                    request.Headers.Add(str);

                request.UserAgent = cookieModel.UserAgent;

                if (_proxyModel != null)
                {
                    var wp = new WebProxy(_proxyModel.ip, _proxyModel.port);
                    wp.Credentials = new NetworkCredential(_proxyModel.username, _proxyModel.password);

                    request.Proxy = wp;
                }

                Uri target = new Uri("http://api.pinterest.com");

                CookieContainer container = new CookieContainer(6);
                container.Add(new Cookie("authorization", cookieModel.Authorization) { Domain = target.Host });

                request.CookieContainer = container;

                if (!string.IsNullOrEmpty(postBody))
                {
                    request.Method = "POST";

                    using (var stream = request.GetRequestStream())
                    {
                        var bytes = ASCIIEncoding.ASCII.GetBytes(postBody);
                        stream.Write(bytes, 0, bytes.Length);
                    }
                }

                WebResponse response = request.GetResponse();

                responseStream.Result = response.GetResponseStream();
                responseStream.StatusCode = 200;
            }
            catch (WebException exc) {
                responseStream.Result = null;
                responseStream.Message = exc.Message;

                int statusCode = 500;
                var response = exc.Response as HttpWebResponse;
                if (response != null)
                {
                    statusCode = (int)response.StatusCode;
                    responseStream.StatusCode = statusCode;
                }

                if (exc.Response != null)
                {
                    var stream = exc.Response.GetResponseStream();
                }
            }
            catch (Exception exc)
            {
                responseStream.Result = null;
                responseStream.Message = exc.Message;
                responseStream.StatusCode = 500;
            }

            return responseStream;
        }
    }

    public class RequestResult {
        public bool IsSucceed { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
    }

    public class RequestStreamResult
    {
        public bool IsSucceed { get; set; }
        public Stream Result { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
    }
}
