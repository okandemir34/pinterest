﻿using System;
using System.Collections.Generic;

namespace Pinterest.Cookie.Entities
{
    public class UserPinsResult
    {
        public string status { get; set; }
        public string bookmark { get; set; }
        public string message { get; set; }
        public int code { get; set; }

        public List<UserPin> data { get; set; }

        public class UserPin {
            public bool requires_advertiser_attribution { get; set; }
            public bool is_ghost { get; set; }
            public string price_currency { get; set; }
            public bool is_eligible_for_web_closeup { get; set; }
            public string title { get; set; }
            public bool is_scene { get; set; }
            public OnlyIdObject board { get; set; }

            public class OnlyIdObject {
                public string id { get; set; }
            }

            public string image_signature { get; set; }
            public bool is_playable { get; set; }
            public string image_square_url { get; set; }
            public int repin_count { get; set; }

            public SizePixel image_medium_size_pixels { get; set; }
            public class SizePixel {
                public int width { get; set; }
                public int height { get; set; }
            }

            public string image_medium_url { get; set; }
            public string closeup_unified_description { get; set; }
            public string closeup_user_note { get; set; }
            public string category { get; set; }
            public string method { get; set; }
            public string closeup_unified_title { get; set; }
            public bool comments_disabled { get; set; }
            public SizePixel image_square_size_pixels { get; set; }
            public string domain { get; set; }
            public SizePixel image_medium_size_points { get; set; }
            public OnlyIdObject board_activity { get; set; }
            public OnlyIdObject pinner { get; set; }
            public string privacy { get; set; }
            public bool is_full_width { get; set; }
            public SizePixel image_square_size_points { get; set; }
            public int comment_count { get; set; }
            public string type { get; set; }
            public string grid_title { get; set; }
            public string link { get; set; }
            public string client_id { get; set; }
            public string tracked_link { get; set; }
            public string[] hashtags { get; set; }
            public int like_count { get; set; }
            public string id { get; set; }
            public SizePixel image_large_size_points { get; set; }
            public string grid_description { get; set; }
            public string image_large_url { get; set; }
            public SizePixel image_large_size_pixels { get; set; }
            public string description { get; set; }
            public bool is_repin { get; set; }
            public string shareable_url { get; set; }
            public bool is_uploaded { get; set; }
            public DateTime created_at { get; set; }
            public string dominant_color { get; set; }
            public bool is_video { get; set; }

            public class Owner
            {
                public string username { get; set; }
                public string first_name { get; set; }
                public string last_name { get; set; }
                public string full_name { get; set; }
                public string type { get; set; }
                public string id { get; set; }
            }

            public Owner third_party_pin_owner { get; set; }
        }
    }
}
