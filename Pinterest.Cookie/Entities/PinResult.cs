﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Pinterest.Cookie.Entities
{
    public class PinResult
    {
        public string status { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public PinDetail data { get; set; }
        

        public class PinDetail
        {
            public string closeup_description { get; set; }
            public string domain { get; set; }
            public bool done_by_me { get; set; }
            public string tracking_params { get; set; }
            public string grid_title { get; set; }

            public class AggregatedPinData
            {
                public string id { get; set; }

                public AggregatedStats aggregated_stats { get; set; }
                public class AggregatedStats
                {
                    public int saves { get; set; }
                    public int done { get; set; }
                }
            }
            public AggregatedPinData aggregated_pin_data { get; set; }

            public class RichSummary
            {
                public string site_name { get; set; }
                public string type_name { get; set; }
                public string display_name { get; set; }
                public string id { get; set; }
            }
            public RichSummary rich_summary { get; set; }
            public string image_signature { get; set; }
            public bool is_eligible_for_aggregated_comments { get; set; }
            public bool is_cpc_ad { get; set; }

            public class ImageAndSize
            {
                public string url { get; set; }
                public int width { get; set; }
                public int height { get; set; }
                public string dominant_color { get; set; }
            }

            public class Images
            {
                [JsonProperty(PropertyName = "345x")]
                public ImageAndSize x345x { get; set; }

                [JsonProperty(PropertyName = "750x")]
                public ImageAndSize x750x { get; set; }

                public ImageAndSize orig { get; set; }
            }
            public Images images { get; set; }

            public string id { get; set; }
            public string category { get; set; }
            public bool is_eligible_for_web_closeup { get; set; }
            public bool is_promoted { get; set; }
            public string title { get; set; }
            public bool promoted_is_max_video { get; set; }
            public int comment_count { get; set; }
            public string cacheable_id { get; set; }
            public string type { get; set; }
            public bool is_whitelisted_for_tried_it { get; set; }
            public string description { get; set; }
            public bool is_shopping_ad { get; set; }
            public string link { get; set; }
            public bool is_repin { get; set; }
            public int ad_match_reason { get; set; }
            public string image_square_url { get; set; }
            public int repin_count { get; set; }
            public string tracked_link { get; set; }
            public DateTime created_at { get; set; }
            public bool is_native { get; set; }
            public bool promoted_is_removable { get; set; }
            public string dominant_color { get; set; }
            public bool can_delete_did_it_and_comments { get; set; }
            public bool is_video { get; set; }
            public bool is_downstream_promotion { get; set; }

            public Pinner pinner { get; set; }
            public class Pinner
            {
                public bool is_downstream_promotion { get; set; }
                public int native_pin_count { get; set; }
                public string first_name { get; set; }
                public string last_name { get; set; }
                public string type { get; set; }
                public bool is_default_image { get; set; }
                public bool blocked_by_me { get; set; }
                public DateTime created_at { get; set; }
                public string image_medium_url { get; set; }
                public string username { get; set; }
                public bool explicitly_followed_by_me { get; set; }
                public string full_name { get; set; }
                public string gender { get; set; }
                public int follower_count { get; set; }
                public string image_small_url { get; set; }

                public class RecentPinImages
                {
                    [JsonProperty(PropertyName = "192x")]
                    public List<ImageAndSize> x192x { get; set; }
                }

                public int pin_count { get; set; }
                public string id { get; set; }
                public string image_large_url { get; set; }
            }

            public Board board { get; set; }
            public class Board
            {
                public bool collaborator_invites_enabled { get; set; }
                public bool is_collaborative { get; set; }
                public string layout { get; set; }
                public string name { get; set; }

                public class CoverImages
                {
                    [JsonProperty(PropertyName = "60x60")]
                    public ImageAndSize x6060 { get; set; }

                    [JsonProperty(PropertyName = "200x")]
                    public ImageAndSize x200 { get; set; }
                }
                public CoverImages cover_images { get; set; }

                public string privacy { get; set; }
                public string url { get; set; }
                public bool should_show_board_activity { get; set; }
                public string image_cover_url { get; set; }
                public int collaborator_count { get; set; }
                public string type { get; set; }
                public bool collaborated_by_me { get; set; }

                public class Images
                {
                    [JsonProperty(PropertyName = "150x150")]
                    public List<ImageAndSize> x150x150 { get; set; }
                }
                public Images images { get; set; }

                public bool followed_by_me { get; set; }
                public int pin_count { get; set; }
                public string id { get; set; }
                public string image_thumbnail_url { get; set; }

                public Owner owner { get; set; }
                public class Owner
                {
                    public string id { get; set; }
                }
            }
        }
    }
}
