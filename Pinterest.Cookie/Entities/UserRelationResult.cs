﻿using System.Collections.Generic;

namespace Pinterest.Cookie.Entities
{
    public class UserRelationResult
    {
        public string status { get; set; }
        public string bookmark { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public List<UserFollower> data { get; set; }

        public class UserFollower
        {
            public string username { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string gender { get; set; }
            public string image_medium_url { get; set; }
            public string image_xlarge_url { get; set; }
            public string full_name { get; set; }
            public string image_small_url { get; set; }
            public string type { get; set; }
            public string id { get; set; }
            public string image_large_url { get; set; }
        }
    }
}
