﻿using System;
using System.Collections.Generic;

namespace Pinterest.Cookie.Entities
{
    public class TagPinsResult
    {
        public string status { get; set; }
        public List<TagTerm> Terms { get; set; }
        public class TagTerm
        {
            public string term { get; set; }
            public string ios_phone_blurred_url { get; set; }
            public int source { get; set; }
            public string dominant_color { get; set; }
            public int position { get; set; }
            public string ios_phone_retina_blurred_url { get; set; }
            public string display { get; set; }
        }

        public string bookmark { get; set; }
        public int code { get; set; }
        public bool pull_to_refresh_enabled { get; set; }
        public string message { get; set; }

        public List<TagPin> data { get; set; }

        public class TagPin {
            public string domain { get; set; }

            public class OnlyIdObject
            {
                public string id { get; set; }
            }

            public class SizePixel
            {
                public int width { get; set; }
                public int height { get; set; }
            }

            public SizePixel image_square_size_pixels { get; set; }
            public string tracking_params { get; set; }
            public string image_medium_url { get; set; }
            public SizePixel image_medium_size_points { get; set; }
            public string id { get; set; }
            public SizePixel image_large_size_points { get; set; }
            public string price_currency { get; set; }
            public string title { get; set; }
            public SizePixel image_square_size_points { get; set; }
            public int comment_count { get; set; }
            public string cacheable_id { get; set; }
            public string type { get; set; }
            public string image_large_url { get; set; }
            public SizePixel image_large_size_pixels { get; set; }
            public string description { get; set; }
            public bool is_playable { get; set; }
            public string link { get; set; }
            public bool is_repin { get; set; }
            public bool is_uploaded { get; set; }
            public string image_square_url { get; set; }
            public int repin_count { get; set; }
            public string tracked_link { get; set; }
            public DateTime created_at { get; set; }
            public SizePixel image_medium_size_pixels { get; set; }
            public bool is_video { get; set; }
        }
    }
}
