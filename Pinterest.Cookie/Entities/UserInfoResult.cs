﻿using System;

namespace Pinterest.Cookie.Entities
{
    public class UserInfoResult
    {
        public string status { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public UserInfo data { get; set; }

        public class UserInfo
        {
            public bool show_creator_profile { get; set; }
            public bool active { get; set; }
            public string impressum_url { get; set; }
            public string last_name { get; set; }
            public bool domain_verified { get; set; }
            public int following_count { get; set; }
            public int profile_reach { get; set; }
            public string image_medium_url { get; set; }
            public bool implicitly_followed_by_me { get; set; }
            public DateTime last_pin_like_time { get; set; }
            public int like_count { get; set; }
            public string image_xlarge_url { get; set; }
            public string full_name { get; set; }
            public string gender { get; set; }
            public Partner partner { get; set; }

            public class Partner {
                public string id { get; set; }
            }

            public string image_small_url { get; set; }
            public string id { get; set; }
            public DateTime last_pin_save_time { get; set; }
            public string domain_url { get; set; }
            public bool explicitly_followed_by_me { get; set; }
            public string location { get; set; }
            public bool indexed { get; set; }
            public bool is_partner { get; set; }
            public bool followed_by_me { get; set; }
            public string type { get; set; }
            public string website_url { get; set; }
            public int board_count { get; set; }
            public string username { get; set; }
            public int native_pin_count { get; set; }
            public string first_name { get; set; }
            public bool profile_discovered_public { get; set; }
            public int pin_count { get; set; }
            public int user_following_count { get; set; }
            public string about { get; set; }
            public DateTime created_at { get; set; }
            public string image_large_url { get; set; }
            public string last_login_country { get; set; }
            public int follower_count { get; set; }
            public bool blocked_by_me { get; set; }
        }
    }
}
