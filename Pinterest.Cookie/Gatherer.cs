﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Pinterest.Cookie.Entities;
using Pinterest.Dto;
using System.Collections.Generic;
using System.Linq;

namespace Pinterest.Cookie
{
    public class Gatherer
    {
        public const string API_URL = "https://api.pinterest.com/v3/";
        public CookieModel _cookieModel { get; set; }
        public ProxyModel _proxyModel { get; set; }

        public Gatherer(CookieModel cookie, ProxyModel proxy = null)
        {
            _cookieModel = cookie;
            _proxyModel = proxy;

            if (_proxyModel == null && !string.IsNullOrEmpty(cookie.ip))
            {
                _proxyModel = new ProxyModel()
                {
                    ip = cookie.ip,
                    password = cookie.password,
                    port = cookie.port,
                    username = cookie.username
                };
            }
        }

        public ResponseDto<UserDto> GetUserInfo(string userId)
        {
            var check = Request($"users/{userId}/", "");
            if (!check.IsSucceed && string.IsNullOrEmpty(check.Result))
                return new ResponseDto<UserDto>(null, false, check.Message);

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            var x = JsonConvert.DeserializeObject<UserInfoResult>(check.Result, jsonSettings);

            var userDto = new UserDto()
            {
                About = x.data.about,
                BoardCount = x.data.board_count,
                CreatedAt = x.data.created_at,
                FollowerCount = x.data.follower_count,
                FollowingCount = x.data.following_count,
                FullName = x.data.full_name,
                LastPinLikeTime = x.data.last_pin_like_time,
                LastPinSaveTime = x.data.last_pin_save_time,
                Location = x.data.location,
                PinCount = x.data.pin_count,
                Username = x.data.username.ToLower(),
                Website = x.data.website_url,
                Avatar = x.data.image_medium_url,
                Id = x.data.id
            };

            return new ResponseDto<UserDto>(userDto, true, "");
        }

        /// <summary>
        /// TODO: json'ı bi yerlere yazsak muthis olur
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResponseDto<List<UserSimpleDto>> GetFollowers(string userId, string logPath = "")
        {
            var check = Request($"users/{userId}/followers/", "");
            if (!check.IsSucceed && string.IsNullOrEmpty(check.Result))
                return new ResponseDto<List<UserSimpleDto>>(new List<UserSimpleDto>(), false, "");

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            var x = JsonConvert.DeserializeObject<UserRelationResult>(check.Result, jsonSettings);

            var userList = x.data.Select(y => new UserSimpleDto()
            {
                AvatarSmall = y.image_small_url,
                AvatarMedium = y.image_medium_url,
                AvatarLarge = y.image_large_url,
                FullName = y.full_name,
                UserId = y.id,
                Username = y.username.ToLower()
            }).ToList();

            if (!string.IsNullOrEmpty(logPath) && System.IO.File.Exists(logPath))
            {
                System.IO.File.WriteAllText(logPath, check.Result);
            }

            return new ResponseDto<List<UserSimpleDto>>(userList, true, "");
        }

        /// <summary>
        /// TODO: json'ı bi yerlere yazsak muthis olur
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResponseDto<List<UserSimpleDto>> GetFollowings(string userId, string logPath = "")
        {
            var check = Request($"users/{userId}/following/", "");
            if (!check.IsSucceed && string.IsNullOrEmpty(check.Result))
                return new ResponseDto<List<UserSimpleDto>>(new List<UserSimpleDto>(), false, "");

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            var x = JsonConvert.DeserializeObject<UserRelationResult>(check.Result, jsonSettings);

            var userList = x.data.Select(y => new UserSimpleDto()
            {
                AvatarSmall = y.image_small_url,
                AvatarMedium = y.image_medium_url,
                AvatarLarge = y.image_large_url,
                FullName = y.full_name,
                UserId = y.id,
                Username = y.username.ToLower()
            }).ToList();

            if (!string.IsNullOrEmpty(logPath) && System.IO.File.Exists(logPath))
            {
                System.IO.File.WriteAllText(logPath, check.Result);
            }

            return new ResponseDto<List<UserSimpleDto>>(userList, true, "");
        }

        public ResponseDto<List<PinDto>> GetPinsByUserId(string userId)
        {
            var check = Request($"users/{userId}/pins/", "");
            if (!check.IsSucceed && string.IsNullOrEmpty(check.Result))
                return new ResponseDto<List<PinDto>>(new List<PinDto>(), false, check.Message);

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            var y = JsonConvert.DeserializeObject<UserPinsResult>(check.Result, jsonSettings);

            var list = y.data.Select(x => new PinDto()
            {
                CreatedAt = x.created_at,
                Description = x.description,
                ImageLarge = new ImageDto(x.image_large_url, x.image_large_size_pixels.width, x.image_large_size_pixels.height),
                ImageMedium = new ImageDto(x.image_medium_url, x.image_medium_size_pixels.width, x.image_medium_size_pixels.height),
                ImageSquare = new ImageDto(x.image_square_url, x.image_square_size_pixels.width, x.image_square_size_pixels.height),
                RepinCount = x.repin_count,
                CommentCount = x.comment_count,
                Title = x.title,
                Id = x.id,
                UserSimple = new UserSimpleDto()
                {
                    AvatarLarge = "",
                    AvatarMedium = "",
                    AvatarSmall = "",
                    FullName = "",
                    UserId = userId,
                    Username = ""
                },
                Owner = x.third_party_pin_owner == null
                ? null
                : new UserSimpleDto()
                {
                    FullName = x.third_party_pin_owner.full_name,
                    UserId = x.third_party_pin_owner.id,
                    Username = x.third_party_pin_owner.username
                }
            }).ToList();

            return new ResponseDto<List<PinDto>>(list, true, "");
        }

        public ResponseDto<TagResponseDto> GetPinsByTag(string tag)
        {
            var check = Request($"search/pins/?auto_correction_disabled=0&rs=typed&base_scheme=https&asterix=true&query=" + tag + "&eq=" + tag + "&disable_personalization=0&fields=pin.done_by_me%2Cpin.mobile_link%2Cpin.tracked_link%2Cpin.tracking_params%2Cpin.grid_title%2Cpin.is_promoted%2Cpin.ad_closeup_behaviors%2Cpin.title%2Cpin.image_signature%2Cpin.view_tags%2Cpin.is_eligible_for_aggregated_comments%2Cpin.dominant_color%2Cpin.board%28%29%2Cpin.type%2Cpin.image_square_url%2Cpin.embed%28%29%2Cpin.promoted_is_removable%2Cpin.domain_tracking_params%2Cpin.is_native%2Cpin.link%2Cpin.attribution%2Cpin.image_crop%2Cpin.rich_summary%28%29%2Cpin.is_whitelisted_for_tried_it%2Cpin.can_delete_did_it_and_comments%2Cpin.promoted_ios_deep_link%2Cpin.promoter%28%29%2Cpin.dark_profile_link%2Cpin.repin_count%2Cpin.additional_hide_reasons%2Cpin.product_pin_data%28%29%2Cpin.creative_types%2Cpin.is_downstream_promotion%2Cpin.promoted_is_max_video%2Cpin.aggregated_pin_data%28%29%2Cpin.native_creator%28%29%2Cpin.is_eligible_for_web_closeup%2Cpin.cacheable_id%2Cpin.is_shopping_ad%2Cpin.is_video%2Cpin.ad_destination_url%2Cpin.source_interest%28%29%2Cpin.ad_match_reason%2Cpin.domain%2Cpin.videos%28%29%2Cpin.description%2Cpin.pinner%28%29%2Cpin.created_at%2Cpin.is_cpc_ad%2Cpin.shopping_flags%2Cpin.category%2Cpin.id%2Cpin.comment_count%2Cuserdiditdata.comment_count%2Cuserdiditdata.recommend_score%2Cuserdiditdata.reaction_by_me%2Cuserdiditdata.id%2Cuserdiditdata.reaction_counts%2Cuserdiditdata.user%28%29%2Cuserdiditdata.image_signatures%2Cuserdiditdata.videos%28%29%2Cuserdiditdata.pin%28%29%2Cuserdiditdata.privacy%2Cuserdiditdata.details%2Cuserdiditdata.recommendation_reason%2Cuserdiditdata.type%2Cuserdiditdata.done_at%2Cvideo.signature%2Cvideo.video_list%5BV_HLSV4%5D%2Cvideo.id%2Cexploretabcoverimage.image_large_url%2Cexploretabcoverimage.type%2Cexploretabcoverimage.id%2Cproductmetadatav2.items%2Citemmetadata.additional_images%2Crichpingriddata.is_product_pin_v2%2Crichpingriddata.products%28%29%2Crichpingriddata.aggregate_rating%2Crichpingriddata.display_cook_time%2Crichpingriddata.type_name%2Crichpingriddata.display_name%2Crichpingriddata.mobile_app%2Crichpingriddata.site_name%2Crichpingriddata.id%2Crichpingriddata.actions%2Caggregatedpindata.aggregated_stats%2Caggregatedpindata.id%2Cembed.type%2Cembed.width%2Cembed.src%2Cembed.height%2Cexplorearticle.curator%28%29%2Cexplorearticle.cover_images%5B236x%2C%20474x%2C%20280x280%5D%2Cexplorearticle.video_cover_pin%28%29%2Cexplorearticle.subtitle%2Cexplorearticle.dominant_colors%2Cexplorearticle.story_category%2Cexplorearticle.show_cover%2Cexplorearticle.aux_fields%2Cexplorearticle.num_videos%2Cexplorearticle.type%2Cexplorearticle.is_shareable%2Cexplorearticle.title%2Cexplorearticle.id%2Cideascard.answer%2Cideascard.completion_subtitle%2Cideascard.feed_metadata%28%29%2Cideascard.background_image_url%2Cideascard.id%2Cideascard.hide_when_offscreen%2Cideascard.completion_button_text%2Cideascard.completion_title%2Cideascard.can_dismiss%2Cideascard.type%2Cideascardfeedmetadata.caption%2Cideascardfeedmetadata.tab_titles%2Cideascardfeedmetadata.feed_type%2Cideascardfeedmetadata.id%2Cideascardfeedmetadata.feed_subtitle%2Cideascardfeedmetadata.feed_title%2Cideascardfeedmetadata.type%2Cideascardfeedmetadata.navigation_bar_title%2Cboard.type%2Cboard.id%2Cboard.is_collaborative%2Cboard.name%2Cboard.followed_by_me%2Cboard.collaborator_count%2Cboard.layout%2Cboard.url%2Cboard.collaborator_invites_enabled%2Cboard.should_show_board_activity%2Cboard.collaborated_by_me%2Cboard.pin_count%2Cboard.privacy%2Cboard.image_thumbnail_url%2Cboard.image_cover_url%2Cboard.owner%28%29%2Cprofessionalservice.id%2Cprofessionalservice.ideas_card%28%29%2Cprofessionalservice.name%2Cprofessionalservice.type%2Cprofessionalservice.callback_cancel_url%2Cprofessionalservice.callback_complete_url%2Cprofessionalservice.external_service_url%2Cprofessionalservice.cover_image_url%2Cuser.recent_pin_images%5B192x%5D%2Cuser.image_medium_url%2Cuser.image_large_url%2Cuser.verified_identity%2Cuser.id%2Cuser.type%2Cuser.show_creator_profile%2Cuser.blocked_by_me%2Cuser.image_small_url%2Cuser.follower_count%2Cuser.full_name%2Cuser.native_pin_count%2Cuser.created_at%2Cuser.is_default_image%2Cuser.first_name%2Cuser.username%2Cuser.explicitly_followed_by_me%2Cuser.pin_count%2Cuser.last_name%2Cuser.gender%2Cinterest.name%2Cinterest.key%2Cinterest.is_followed%2Cinterest.type%2Cinterest.follower_count%2Cinterest.id%2Cuserdiditdata.images%5B1080x%2C345x%2Corig%5D%2Cboard.images%5B150x150%5D%2Cboard.cover_images%5B60x60%2C200x%5D%2Cimagemetadata.canonical_images%5B474x%2C1200x%5D%2Cpin.images%5B750x%2C345x%2Corig%5D%2Cinterest.images%5B70x70%2C236x%5D&term_meta=%28%0A%20%20%20%20%22" + tag + "%7Ctyped%22%0A%29&etslf=10772&item_count=0&dynamic_grid_stories=6&enable_promoted_pins=1&page_size=50", "");
            if (!check.IsSucceed && string.IsNullOrEmpty(check.Result))
                return new ResponseDto<TagResponseDto>(new TagResponseDto(), false, check.Message);

            var json = JObject.Parse(check.Result);
            var jArr = JArray.Parse(json["data"].ToString());
            var list = new List<RelatedPin>();

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            foreach (var item in jArr)
            {
                if (item["type"].ToString() == "story")
                    continue;

                var y = JsonConvert.DeserializeObject<RelatedPin>(item.ToString(), jsonSettings);
                list.Add(y);
            }

            var pinsDto = list.Select(x => new PinDto()
            {
                Category = x.category,
                CommentCount = x.comment_count,
                CreatedAt = x.created_at,
                Description = x.description,
                Id = x.id,
                ImageLarge = new ImageDto(x.images.x750x.url, x.images.x750x.width, x.images.x750x.height),
                ImageMedium = new ImageDto(x.images.x345x.url, x.images.x345x.width, x.images.x345x.height),
                IsRepin = x.is_repin,
                Link = x.link,
                Owner = new UserSimpleDto()
                {
                    AvatarLarge = "",
                    AvatarMedium = "",
                    AvatarSmall = "",
                    FullName = "",
                    UserId = x.board == null || x.board.owner == null ? "" : x.board.owner.id,
                    Username = ""
                },
                RepinCount = x.repin_count,
                Title = x.title,
                UserSimple = new UserSimpleDto()
                {
                    AvatarLarge = x.pinner.image_large_url,
                    AvatarMedium = x.pinner.image_medium_url,
                    AvatarSmall = x.pinner.image_small_url,
                    FullName = x.pinner.full_name,
                    UserId = x.pinner.id,
                    Username = x.pinner.username
                }
            }).ToList();

            //termsi
            var listTerms = new List<TermDto>();
            if (json["terms"] != null)
            {
                var jArrTerms = JArray.Parse(json["terms"].ToString());
                foreach (var item in jArrTerms)
                {
                    var term = new TermDto()
                    {
                        Color = item["dominant_color"].ToString(),
                        Name = item["display"].ToString(),
                        Term = item["term"].ToString(),
                    };

                    listTerms.Add(term);
                }
            }

            var tagResponse = new TagResponseDto()
            {
                Pins = pinsDto,
                Tag = tag,
                Terms = listTerms
            };

            return new ResponseDto<TagResponseDto>(tagResponse, true, "");
        }

        public ResponseDto<PinDto> GetPinById(string id)
        {
            var check = Request($"pins/" + id + "/?base_scheme=https&client_tracking_params=CwABAAAADDc5NjYyNDA2NjQ5MAoAAgAAAWXACaY9AA&fields=pin.domain_tracking_params%2Cpin.origin_pinner%28%29%2Cpin.source_interest%28%29%2Cpin.visual_objects%28%29%2Cpin.domain%2Cpin.closeup_description%2Cpin.promoted_is_removable%2Cpin.origin_pin%28%29%2Cpin.tracking_params%2Cpin.is_repin%2Cpin.grid_title%2Cpin.promoted_ios_deep_link%2Cpin.section%28%29%2Cpin.is_eligible_for_aggregated_comments%2Cpin.board%28%29%2Cpin.board_activity%28%29%2Cpin.id%2Cpin.attribution%2Cpin.rich_metadata%28%29%2Cpin.click_through_action%2Cpin.ad_closeup_behaviors%2Cpin.cacheable_id%2Cpin.rich_summary%28%29%2Cpin.can_delete_did_it_and_comments%2Cpin.image_signature%2Cpin.comment_count%2Cpin.did_it_disabled%2Cpin.view_tags%2Cpin.link%2Cpin.native_creator%28%29%2Cpin.comments_disabled%2Cpin.created_at%2Cpin.is_promoted%2Cpin.is_native%2Cpin.ad_destination_url%2Cpin.pinned_to_board%28%29%2Cpin.dark_profile_link%2Cpin.image_square_url%2Cpin.via_pinner%28%29%2Cpin.visual_search_attrs%2Cpin.is_eligible_for_web_closeup%2Cpin.additional_hide_reasons%2Cpin.link_domain%28%29%2Cpin.description%2Cpin.done_by_me%2Cpin.videos%28%29%2Cpin.is_cpc_ad%2Cpin.dominant_color%2Cpin.title%2Cpin.is_downstream_promotion%2Cpin.image_crop%2Cpin.embed%28%29%2Cpin.creative_types%2Cpin.aggregated_pin_data%28%29%2Cpin.promoted_is_max_video%2Cpin.is_whitelisted_for_tried_it%2Cpin.pinner%28%29%2Cpin.ad_match_reason%2Cpin.tracked_link%2Cpin.is_video%2Cpin.shopping_flags%2Cpin.product_pin_data%28%29%2Cpin.repin_count%2Cpin.mobile_link%2Cpin.type%2Cpin.is_shopping_ad%2Cpin.promoter%28%29%2Cpin.buyable_product%28%29%2Cpin.category%2Caggregatedpindata.id%2Caggregatedpindata.comment_count%2Caggregatedpindata.aggregated_stats%2Caggregatedpindata.did_it_data%28%29%2Ccommunitycontent.tags%2Ccommunitycontent.text%2Ccommunitycontent.type%2Ccommunitycontent.id%2Ccommunitycontent.image_signatures%2Ccommunitycontent.object_ids%2Cembed.type%2Cembed.width%2Cembed.src%2Cembed.height%2Cinterest.name%2Cinterest.key%2Cinterest.is_followed%2Cinterest.type%2Cinterest.follower_count%2Cinterest.id%2Cvideo.video_list%5BV_HLSV4%5D%2Cvideo.id%2Cbuyableproductmetadata.display_active_max_price%2Cbuyableproductmetadata.merchant_id%2Cbuyableproductmetadata.merchant_name%2Cbuyableproductmetadata.title%2Cbuyableproductmetadata.id%2Cbuyableproductmetadata.short_description%2Cbuyableproductmetadata.merchant_item_group_id%2Cbuyableproductmetadata.display_active_min_price%2Cbuyableproductmetadata.is_available%2Cboardactivitycomment.content%2Cboardactivitycomment.user%28%29%2Cboardactivitycomment.parent_comment%28%29%2Cboardactivitycomment.type%2Cboardactivitycomment.reaction_counts%2Cboardactivitycomment.created_at%2Cboardactivitycomment.last_edited%2Cboardactivitycomment.comment_count%2Cboardactivitycomment.id%2Cboardactivitycomment.tagged_users%28%29%2Cboardactivitycomment.reaction_by_me%2Cboardactivitycomment.depth%2Cboardactivitycomment.root_board_activity%28%29%2Cboardsection.type%2Cboardsection.board%2Cboardsection.board%28%29%2Cboardsection.preview_pins%28%29%2Cboardsection.pin_count%2Cboardsection.id%2Cboardsection.title%2Cmakecardtutorialview.partner%28%29%2Cuser.image_medium_url%2Cuser.image_large_url%2Cuser.id%2Cuser.type%2Cuser.show_creator_profile%2Cuser.username%2Cuser.image_small_url%2Cuser.full_name%2Cuser.native_pin_count%2Cuser.first_name%2Cuser.is_default_image%2Cuser.explicitly_followed_by_me%2Cuser.gender%2Cuser.last_name%2Cdomain.name%2Cdomain.id%2Cdomain.official_user%28%29%2Ccommunity.name%2Ccommunity.permissions%2Ccommunity.type%2Ccommunity.roles%2Ccommunity.id%2Ccommunity.members_count%2Ccommunity.slug%2Ccommunity.preview_members%28%29%2Ccommunity.banned_count%2Ccommunity.is_member%2Ccommunity.cover_image_signature%2Ccommunity.description%2Ccommunity.created_at%2Crichpindataview.products%2Crichpindataview.title%2Crichpindataview.tutorial%2Crichpindataview.mobile_app%2Crichpindataview.is_product_pin_v2%2Crichpindataview.recipe%2Crichpindataview.article%2Crichpindataview.id%2Crichpindataview.url%2Citemmetadata.additional_images%2Ccommunitypost.comment_count%2Ccommunitypost.reaction_by_me%2Ccommunitypost.sticky%2Ccommunitypost.title%2Ccommunitypost.community%28%29%2Ccommunitypost.slug%2Ccommunitypost.user%28%29%2Ccommunitypost.last_edited%2Ccommunitypost.type%2Ccommunitypost.reaction_counts%2Ccommunitypost.pins%28%29%2Ccommunitypost.views%2Ccommunitypost.visibility%2Ccommunitypost.tagged_users%28%29%2Ccommunitypost.id%2Ccommunitypost.content%2Ccommunitypost.created_at%2Ccommunitypost.community%2Cboard.type%2Cboard.is_collaborative%2Cboard.id%2Cboard.created_at%2Cboard.followed_by_me%2Cboard.url%2Cboard.name%2Cboard.section_count%2Cboard.layout%2Cboard.collaborator_invites_enabled%2Cboard.should_show_board_activity%2Cboard.collaborated_by_me%2Cboard.category%2Cboard.owner%2Cboard.privacy%2Cboard.image_thumbnail_url%2Cboard.conversation%2Cboard.image_cover_url%2Cboard.owner%28%29%2Crichpingriddata.is_product_pin_v2%2Crichpingriddata.products%28%29%2Crichpingriddata.aggregate_rating%2Crichpingriddata.display_cook_time%2Crichpingriddata.type_name%2Crichpingriddata.display_name%2Crichpingriddata.mobile_app%2Crichpingriddata.site_name%2Crichpingriddata.id%2Crichpingriddata.actions%2Cboardactivity.user%28%29%2Cboardactivity.pin%28%29%2Cboardactivity.type%2Cboardactivity.activity_type%2Cboardactivity.id%2Cboardactivity.aux_data%2Cboardactivity.content%2Cboardactivity.aggregate_pin_count%2Cboardactivity.comment_count%2Cboardactivity.reaction_counts%2Cboardactivity.created_at%2Cboardactivity.last_edited%2Cboardactivity.board%28%29%2Cboardactivity.comment%28%29%2Cboardactivity.section%28%29%2Cboardactivity.reaction_by_me%2Cboardactivity.ref_board_activity%28%29%2Cproductmetadatav2.items%2Ccommunity.cover_images%5B1200x%2C750x%5D%2Cboard.cover_images%5B60x60%2C200x%5D%2Cpin.images%5B750x%2Corig%2C345x%5D%2Cinterest.images%5B70x70%2C236x%5D%2Ccommunitycontent.images%5B750x%2C1200x%5D%2Cimagemetadata.canonical_images%5B474x%2C1200x%2Corig%5D%2Cmakecardtutorialinstructionview.images%5B550x%2C736x%2C236x%5D%2Cmakecardtutorialview.images%5B550x%2C736x%2C236x%5D&page_size=60", "");
            if (!check.IsSucceed && string.IsNullOrEmpty(check.Result))
                return new ResponseDto<PinDto>(null, false, "");

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            var tmp = JsonConvert.DeserializeObject<PinResult>(check.Result, jsonSettings);
            var pin = new PinDto()
            {
                CreatedAt = tmp.data.created_at,
                Description = string.IsNullOrEmpty(tmp.data.closeup_description)
                        ? tmp.data.description
                        : tmp.data.closeup_description,
                Title = tmp.data.grid_title != null && tmp.data.grid_title.Length > tmp.data.title.Length
                    ? tmp.data.grid_title
                    : tmp.data.title,
                Id = tmp.data.id,
                ImageLarge = new ImageDto(tmp.data.images.x750x.url, tmp.data.images.x750x.width, tmp.data.images.x750x.height),
                ImageMedium = new ImageDto(tmp.data.images.x345x.url, tmp.data.images.x345x.width, tmp.data.images.x345x.height),
                RepinCount = tmp.data.repin_count,
                CommentCount = tmp.data.comment_count,
                IsRepin = tmp.data.is_repin,
                Link = tmp.data.link,
                Category = tmp.data.category,
                UserSimple = new UserSimpleDto()
                {
                    AvatarSmall = tmp.data.pinner.image_small_url,
                    AvatarMedium = tmp.data.pinner.image_medium_url,
                    AvatarLarge = tmp.data.pinner.image_large_url,
                    FullName = tmp.data.pinner.full_name,
                    UserId = tmp.data.pinner.id,
                    Username = tmp.data.pinner.username
                }
            };

            return new ResponseDto<PinDto>(pin, true, "");
        }

        public ResponseDto<List<PinDto>> GetRelatedPins(string id)
        {
            var url = "pins/" + id + "/related/pin/?source=user&fields=pin.domain_tracking_params%2Cpin.source_interest%28%29%2Cpin.domain%2Cpin.promoted_is_removable%2Cpin.tracking_params%2Cpin.is_repin%2Cpin.grid_title%2Cpin.promoted_ios_deep_link%2Cpin.is_eligible_for_aggregated_comments%2Cpin.board%28%29%2Cpin.id%2Cpin.attribution%2Cpin.ad_closeup_behaviors%2Cpin.cacheable_id%2Cpin.rich_summary%28%29%2Cpin.can_delete_did_it_and_comments%2Cpin.image_signature%2Cpin.comment_count%2Cpin.view_tags%2Cpin.link%2Cpin.is_native%2Cpin.native_creator%28%29%2Cpin.created_at%2Cpin.is_promoted%2Cpin.ad_destination_url%2Cpin.dark_profile_link%2Cpin.image_square_url%2Cpin.is_eligible_for_web_closeup%2Cpin.additional_hide_reasons%2Cpin.description%2Cpin.done_by_me%2Cpin.videos%28%29%2Cpin.is_cpc_ad%2Cpin.dominant_color%2Cpin.title%2Cpin.is_downstream_promotion%2Cpin.embed%28%29%2Cpin.image_crop%2Cpin.creative_types%2Cpin.aggregated_pin_data%28%29%2Cpin.promoted_is_max_video%2Cpin.is_whitelisted_for_tried_it%2Cpin.pinner%28%29%2Cpin.ad_match_reason%2Cpin.tracked_link%2Cpin.is_video%2Cpin.shopping_flags%2Cpin.product_pin_data%28%29%2Cpin.repin_count%2Cpin.mobile_link%2Cpin.type%2Cpin.is_shopping_ad%2Cpin.promoter%28%29%2Cpin.category%2Cuserdiditdata.comment_count%2Cuserdiditdata.recommend_score%2Cuserdiditdata.reaction_by_me%2Cuserdiditdata.id%2Cuserdiditdata.reaction_counts%2Cuserdiditdata.user%28%29%2Cuserdiditdata.image_signatures%2Cuserdiditdata.pin%28%29%2Cuserdiditdata.privacy%2Cuserdiditdata.details%2Cuserdiditdata.recommendation_reason%2Cuserdiditdata.type%2Cuserdiditdata.done_at%2Cvideo.video_list%5BV_HLSV4%5D%2Cvideo.id%2Cexploretabcoverimage.image_large_url%2Cexploretabcoverimage.type%2Cexploretabcoverimage.id%2Cproductmetadatav2.items%2Citemmetadata.additional_images%2Crichpingriddata.is_product_pin_v2%2Crichpingriddata.products%28%29%2Crichpingriddata.aggregate_rating%2Crichpingriddata.display_cook_time%2Crichpingriddata.type_name%2Crichpingriddata.display_name%2Crichpingriddata.mobile_app%2Crichpingriddata.site_name%2Crichpingriddata.id%2Crichpingriddata.actions%2Caggregatedpindata.aggregated_stats%2Caggregatedpindata.id%2Cembed.type%2Cembed.width%2Cembed.src%2Cembed.height%2Cexplorearticle.curator%28%29%2Cexplorearticle.cover_images%5B236x%2C%20474x%2C%20280x280%5D%2Cexplorearticle.video_cover_pin%28%29%2Cexplorearticle.subtitle%2Cexplorearticle.dominant_colors%2Cexplorearticle.story_category%2Cexplorearticle.show_cover%2Cexplorearticle.aux_fields%2Cexplorearticle.num_videos%2Cexplorearticle.type%2Cexplorearticle.is_shareable%2Cexplorearticle.title%2Cexplorearticle.id%2Cboard.type%2Cboard.id%2Cboard.is_collaborative%2Cboard.name%2Cboard.followed_by_me%2Cboard.collaborator_count%2Cboard.layout%2Cboard.url%2Cboard.collaborator_invites_enabled%2Cboard.should_show_board_activity%2Cboard.collaborated_by_me%2Cboard.pin_count%2Cboard.privacy%2Cboard.image_thumbnail_url%2Cboard.image_cover_url%2Cboard.owner%28%29%2Cuser.recent_pin_images%5B192x%5D%2Cuser.image_medium_url%2Cuser.image_large_url%2Cuser.id%2Cuser.type%2Cuser.show_creator_profile%2Cuser.blocked_by_me%2Cuser.image_small_url%2Cuser.follower_count%2Cuser.full_name%2Cuser.native_pin_count%2Cuser.created_at%2Cuser.is_default_image%2Cuser.first_name%2Cuser.username%2Cuser.explicitly_followed_by_me%2Cuser.pin_count%2Cuser.gender%2Cuser.last_name%2Cinterest.name%2Cinterest.key%2Cinterest.is_followed%2Cinterest.type%2Cinterest.follower_count%2Cinterest.id%2Cuserdiditdata.images%5B1080x%2C345x%2Corig%5D%2Cboard.images%5B150x150%5D%2Cboard.cover_images%5B60x60%2C200x%5D%2Cimagemetadata.canonical_images%5B474x%2C1200x%5D%2Cpin.images%5B750x%2C345x%2Corig%5D%2Cinterest.images%5B70x70%2C236x%5D&bookmark=Y2JVSG81UkZvd1JrTlJWVVpDVVd4d2Frc3hSbGhhUjA1S1VWVkdTbEZWUmtKUlZWcENXakJHUmxGVlJrSlFXSGQzV1ZSQmVsbHFZekZQUjBwb1RYcEZlazlVVm1oT2Ftc3hXbXBvYWxwSFNURk9SMVp0VG5wc2JWcEhTVE5aVjFGNFRtMVNhbHBVUVhwT2VrMHlUbFJqZVUxRVdURk5iVlpvV2tSbk1GbFhVWHBhUjAwdzpVSG81VDJJeU5XeG1SRTVyVDFSTk1GcFVRVEpQUkdzMVRUSlJlRTR5VlRWYVJFVXpUMGRPYkZsVVNUUlpWMXB0VGxSbk5VNTZTWGhhYWxwb1RucHNiRTVFUVhoYVZFRjVXa2RaTWs0eVJUUk5lbWhxV1ZSYWEwOVhVWGhhUkZrOXw0MzJjZDdlODQzMGIzZjM1ZTFkOTk5Nzk4NzRhNmY0OTg3Mjc0NDZhMGI0MzVhYTlmY2U0MmZiNDc4ZGYxNjc4&top_level_source=user&base_scheme=https&item_count=5&top_level_source_depth=1&force_ads_insertion=true&dynamic_grid_stories=6&page_size=50"; ;

            var check = Request(url, "");
            if (!check.IsSucceed && string.IsNullOrEmpty(check.Result))
                return new ResponseDto<List<PinDto>>(new List<PinDto>(), false, check.Message);

            var json = JObject.Parse(check.Result);
            var jArr = JArray.Parse(json["data"].ToString());
            var list = new List<RelatedPin>();

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            foreach (var item in jArr)
            {
                if (item["type"].ToString() == "story")
                    continue;

                var y = JsonConvert.DeserializeObject<RelatedPin>(item.ToString(), jsonSettings);
                list.Add(y);
            }

            var pinsDto = list.Select(x => new PinDto()
            {
                Category = x.category,
                CommentCount = x.comment_count,
                CreatedAt = x.created_at,
                Description = x.description,
                Id = x.id,
                ImageLarge = new ImageDto(x.images.x750x.url, x.images.x750x.width, x.images.x750x.height),
                ImageMedium = new ImageDto(x.images.x345x.url, x.images.x345x.width, x.images.x345x.height),
                IsRepin = x.is_repin,
                Link = x.link,
                Owner = new UserSimpleDto()
                {
                    AvatarLarge = "",
                    AvatarMedium = "",
                    AvatarSmall = "",
                    FullName = "",
                    UserId = x.board == null || x.board.owner == null ? "" : x.board.owner.id,
                    Username = ""
                },
                RepinCount = x.repin_count,
                Title = x.title,
                UserSimple = new UserSimpleDto()
                {
                    AvatarLarge = x.pinner.image_large_url,
                    AvatarMedium = x.pinner.image_medium_url,
                    AvatarSmall = x.pinner.image_small_url,
                    FullName = x.pinner.full_name,
                    UserId = x.pinner.id,
                    Username = x.pinner.username
                }
            }).ToList();

            return new ResponseDto<List<PinDto>>(pinsDto, true, "");
        }

        public ResponseDto<SuggestionResponseDto> GetSuggestions(string query)
        {
            var url = "search/autocomplete/?fields=user.is_default_image%2Cuser.full_name%2Cuser.username%2Cuser.image_medium_url%2Cuser.id%2Cuser.type%2Cuser.verified_identity%2Cuser.native_pin_count%2Cboard.image_thumbnail_url%2Cboard.owner%28%29%2Cboard.should_show_more_ideas%2Cboard.type%2Cboard.name%2Cboard.id%2Cboard.should_show_board_activity&num_boards=3&num_autocompletes=8&q="+ query +"&base_scheme=https&num_people=3&tags=mutual_follow%2Cfollowee%2Cfacebook_pinner%2Ctwitter_pinner%2Cpin_suggestion%2Csecond_degree_followee";
            var check = Request(url, "");
            if (!check.IsSucceed && string.IsNullOrEmpty(check.Result))
                return new ResponseDto<SuggestionResponseDto>(new SuggestionResponseDto(), false, check.Message);

            var json = JObject.Parse(check.Result);
            var jArr = JArray.Parse(json["data"].ToString());
            var terms = new List<TermDto>();
            var userSimples = new List<UserSimpleDto>();

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            //termsi
            for (int i = 0; i < jArr.Count; i++)
            {
                var username = jArr[i]["username"];
                if (username != null)
                {
                    userSimples.Add(new UserSimpleDto()
                    {
                        AvatarLarge = jArr[i]["image_medium_url"].ToString(),
                        AvatarMedium = jArr[i]["image_medium_url"].ToString(),
                        AvatarSmall = jArr[i]["image_small_url"].ToString(),
                        FullName = jArr[i]["full_name"].ToString(),
                        UserId = jArr[i]["id"].ToString(),
                        Username = jArr[i]["username"].ToString()
                    });

                    continue;
                }

                var term = jArr[i]["query"];
                if (term == null)
                    continue;

                terms.Add(new TermDto()
                {
                    Color = "#00FF00",
                    Name = "",
                    Term = term.ToString()
                });
            }

            var suggestion = new SuggestionResponseDto()
            {
                Terms = terms,
                Users = userSimples,
                Term = query
            };

            return new ResponseDto<SuggestionResponseDto>(suggestion, true, "");
        }

        protected RequestResult Request(string endpoint, string postBody)
        {
            var httpVar = new Http(_proxyModel);
            var _headers = new List<string>()
            {
            };
            var response = httpVar.Request(API_URL + endpoint, _headers, postBody, _cookieModel);

            return response;
        }

       
    }
}
