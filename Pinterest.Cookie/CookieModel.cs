﻿namespace Pinterest.Cookie
{
    public class CookieModel
    {
        public string Authorization { get; set; }
        public string UserAgent { get; set; }

        //her cookie kendi proxy modeline sahip olabilir
        public string ip { get; set; }
        public int port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
