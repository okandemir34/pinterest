﻿namespace Pinterest.Cookie
{
    public class ProxyModel
    {
        public string ip { get; set; }
        public int port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
