﻿using System;

namespace SomePinte.Model
{
    public class BanUser : Core.ModelBase
    {
        public string UserId { get; set; }
        public string UserName { get; set; }

        /// <summary>
        /// 1- No Ads 2- Redirect
        /// </summary>
        public int Type { get; set; }
        public DateTime CreateDate { get; set; }
        public string Description { get; set; }
    }
}
