﻿namespace SomePinte.Model
{
    public class UserInfo : Core.ModelBase
    {
        public string Username { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string About { get; set; }
        public int Pins { get; set; }
        public int Followers { get; set; }
        public int Followings { get; set; }
        public int Boards { get; set; }
        public string Avatar { get; set; }
        public string LastImagePath { get; set; }
    }
}
