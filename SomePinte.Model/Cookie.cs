﻿using System;

namespace SomePinte.Model
{
    public class Cookie : Core.ModelBase
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string UserAgent { get; set; }
        public bool IsActive { get; set; }
    }
}
