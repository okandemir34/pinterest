﻿using System;

namespace SomePinte.Model
{
    public class BanWord : Core.ModelBase
    {
        public string Word { get; set; }
        
        //1- No Ads 2- Redirect
        public int Type { get; set; }
        public DateTime CreateDate { get; set; }
        public string Description { get; set; }
    }
}
