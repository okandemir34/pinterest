﻿namespace SomePinte.Model
{
    public class PossibleUser : Core.ModelBase
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool HasChecked { get; set; }
        public bool HasLogin { get; set; }
    }
}
