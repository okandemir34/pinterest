﻿using System;

namespace SomePinte.Model
{
    public class CountryUser : Core.ModelBase
    {
        public int LocationId { get; set; }
        public string Username { get; set; }
        public string UserId { get; set; }
        public string Fullname { get; set; }
        public string ImagePath { get; set; }
        public int Followers { get; set; }
        public int Followings { get; set; }
        public int PinCount { get; set; }
        public int BoardCount { get; set; }
    }
}
