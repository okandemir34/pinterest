﻿using System;

namespace SomePinte.Model
{
    public class Message : Core.ModelBase
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
