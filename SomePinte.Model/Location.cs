﻿using System;

namespace SomePinte.Model
{
    public class Location : Core.ModelBase
    {
        public string LocationName { get; set; }
        public string Slug { get; set; }
    }
}
