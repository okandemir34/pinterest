﻿using System;

namespace Pinterest.Dto
{
    public class PinDto
    {
        public UserSimpleDto UserSimple { get; set; }

        public int RepinCount { get; set; }
        public int CommentCount { get; set; }

        public string Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ImageDto ImageLarge { get; set; }
        public ImageDto ImageMedium { get; set; }
        public ImageDto ImageSquare { get; set; }

        //varsa gercek pin sahibi
        public UserSimpleDto Owner { get; set; }

        public bool IsRepin { get; set; }
        public string Link { get; set; }
        public string Category { get; set; }
    }
}
