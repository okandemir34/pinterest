﻿namespace Pinterest.Dto
{
    public class TermDto
    {
        public string Term { get; set; }
        public string Color { get; set; }
        public string Name { get; set; }
    }
}
