﻿using System;

namespace Pinterest.Dto
{
    public class UserSimpleDto
    {
        public string AvatarSmall { get; set; }
        public string AvatarMedium { get; set; }
        public string AvatarLarge { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string UserId { get; set; }
    }
}
