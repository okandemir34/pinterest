﻿using System.Collections.Generic;

namespace Pinterest.Dto
{
    public class SuggestionResponseDto
    {
        public SuggestionResponseDto()
        {
            Terms = new List<TermDto>();
            Users = new List<UserSimpleDto>();
        }

        public string Term { get; set; }
        public List<TermDto> Terms { get; set; }
        public List<UserSimpleDto> Users { get; set; }
    }
}
