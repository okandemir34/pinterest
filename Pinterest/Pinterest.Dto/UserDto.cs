﻿using System;

namespace Pinterest.Dto
{
    public class UserDto
    {
        public UserDto()
        {

        }

        public UserDto(string userId, string userName, string fullName, string about)
        {
            Id = userId;
            Username = userName;
            FullName = fullName;
            About = about;
        }

        public string Id { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Location { get; set; }
        public int FollowerCount { get; set; }
        public int FollowingCount { get; set; }
        public int BoardCount { get; set; }
        public int PinCount { get; set; }
        public DateTime LastPinLikeTime { get; set; }
        public DateTime LastPinSaveTime { get; set; }
        public string Website { get; set; }
        public string About { get; set; }
        public string Avatar { get; set; }
    }
}
