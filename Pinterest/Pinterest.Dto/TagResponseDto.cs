﻿using System.Collections.Generic;

namespace Pinterest.Dto
{
    public class TagResponseDto
    {
        public string Tag { get; set; }
        public string Bookmark { get; set; }
        public List<TermDto> Terms { get; set; }
        public List<PinDto> Pins { get; set; }
    }
}
