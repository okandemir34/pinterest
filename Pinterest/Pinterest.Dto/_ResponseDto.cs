﻿namespace Pinterest.Dto
{
    public class ResponseDto<T>
    {
        public ResponseDto(T t, bool isSucceed, string message)
        {
            Data = t;
            Message = message;
            IsSucceed = isSucceed;
        }

        public T Data { get; set; }
        public bool IsSucceed { get; set; }
        public string Message { get; set; }
    }
}
