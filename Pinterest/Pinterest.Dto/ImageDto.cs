﻿using System;

namespace Pinterest.Dto
{
    public class ImageDto
    {
        public ImageDto(string path, int width, int height)
        {
            Path = path;
            Width = width;
            Height = height;
        }

        public string Path { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
