﻿namespace SomePinte.Data.MySql
{
    using Core;

    public class BanMediaData : EntityBaseData<Model.BanMedia>
    {
        public BanMediaData()
            : base(new DataContext())
        {
        }
    }
}
