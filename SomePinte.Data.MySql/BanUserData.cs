﻿namespace SomePinte.Data.MySql
{
    using Core;

    public class BanUserData : EntityBaseData<Model.BanUser>
    {
        public BanUserData()
            : base(new DataContext())
        {
        }
    }
}
