﻿namespace SomePinte.Data.MySql
{
    using Core;

    public class LocationData : EntityBaseData<Model.Location>
    {
        public LocationData()
            : base(new DataContext())
        {
        }
    }
}
