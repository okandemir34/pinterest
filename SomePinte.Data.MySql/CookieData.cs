﻿namespace SomePinte.Data.MySql
{
    using Core;

    public class CookieData : EntityBaseData<Model.Cookie>
    {
        public CookieData()
            : base(new DataContext())
        {
        }
    }
}
