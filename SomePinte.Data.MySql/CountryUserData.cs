﻿namespace SomePinte.Data.MySql
{
    using Core;

    public class CountryUserData : EntityBaseData<Model.CountryUser>
    {
        public CountryUserData()
            : base(new DataContext())
        {
        }
    }
}
