﻿namespace SomePinte.Data.MySql
{
    using System;
    using System.Linq;
    using Core;
    using SomePinte.Model;

    public class UserInfoData : EntityBaseData<Model.UserInfo>
    {
        public UserInfoData()
            : base(new DataContext())
        {
        }

        public DataResult Upsert(UserInfo userInfo)
        {
            var infoInDb = GetBy(x => x.UserId == userInfo.UserId).FirstOrDefault();
            if (infoInDb == null)
                return Insert(userInfo);

            //update onemli ama su an degil
            //return Update(userInfo);

            return new DataResult(true, "");
        }
    }
}
