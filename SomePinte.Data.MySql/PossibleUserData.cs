﻿namespace SomePinte.Data.MySql
{
    using Core;

    public class PossibleUserData : EntityBaseData<Model.PossibleUser>
    {
        public PossibleUserData()
            : base(new DataContext())
        {
        }
    }
}
