﻿namespace SomePinte.Data.MySql
{
    using Core;

    public class MessageData : EntityBaseData<Model.Message>
    {
        public MessageData()
            : base(new DataContext())
        {
        }
    }
}
