﻿namespace SomePinte.Data.MySql.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Linq.Dynamic;
    using Model.Core;
    using System.Data.Entity.Validation;

    public class EntityBaseData<T> : IData<T> where T : Model.Core.ModelBase
    {
        protected readonly DbContext _context;

        public EntityBaseData(DbContext context)
        {
            _context = context;
        }

        protected virtual void BeforeUpdate() { }
        protected virtual void AfterUpdate() { }
        protected virtual void BeforeInsert() { }
        protected virtual void AfterInsert() { }
        protected virtual void BeforeDelete() { }
        protected virtual void AfterDelete() { }

        #region IData Implementation

        /// <summary>
        /// Insert new Entity
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public DataResult Insert(T t)
        {
            try
            {
                BeforeInsert();
                _context.Set<T>().Add(t);
                AfterInsert();

                _context.SaveChanges();

                return new DataResult(true, "");
            }
            catch (DbEntityValidationException ex)
            {
                string message = "";
                foreach (var error in ex.EntityValidationErrors)
                {
                    message += string.Format("\nEntity {0} in state {1} has validation errors:",
                        error.Entry.Entity.GetType().Name,
                        error.Entry.State);

                    foreach (var ve in error.ValidationErrors)
                    {
                        message += "\n" + string.Format("\tProperty: {0}, Error: {1}", ve.PropertyName, ve.ErrorMessage);
                    }
                }

                return new DataResult(false, message);
            }
            catch (Exception exc)
            {
                return new DataResult(false, exc.Message +
                    exc.InnerException == null ? "" : "(" + exc.InnerException + ")"
                );
            }
        }

        public DataResult InsertBulk(List<T> ts)
        {
            foreach (var item in ts)
                _context.Set<T>().Add(item);

            _context.SaveChanges();

            return new DataResult(true, "");
        }

        /// <summary>
        /// Update Entity
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public DataResult Update(T t)
        {
            try
            {
                int updateId = t.Id;

                T aModel = _context.Set<T>().Where(x => x.Id == updateId).FirstOrDefault();

                if (aModel == null)
                    return new DataResult(false, "Güncelleme yapılacak kayıt bulunamıyor");

                BeforeUpdate();

                foreach (var propertyInfo in typeof(T).GetProperties())
                {
                    propertyInfo.SetValue(aModel, propertyInfo.GetValue(t, null), null);
                }

                AfterUpdate();

                _context.SaveChanges();

                return new DataResult(true, "");
            }
            catch (Exception exc)
            {
                return new DataResult(false, exc.Message +
                    exc.InnerException == null ? "" : "(" + exc.InnerException + ")"
                );
            }
        }

        public DataResult UpdateBulk(List<T> ts, List<string> properties, List<object> values)
        {
            var typeOfT = typeof(T);
            foreach (var item in ts)
            {
                T aModel = _context.Set<T>().Where(x => x.Id == item.Id).FirstOrDefault();
                if (aModel == null)
                    continue;

                foreach (var prName in properties)
                {
                    for (int i = 0; i < properties.Count; i++)
                        typeOfT.GetProperty(properties[i]).SetValue(item, values[i], null);
                }
            }

            _context.SaveChanges();

            return new DataResult(true, "");
        }

        public DataResult UpdateBulk(Expression<Func<T, bool>> predicate, List<string> properties, List<object> values)
        {
            var list = _context.Set<T>().Where(predicate);
            var typeOfT = typeof(T);
            foreach (var item in list)
            {
                foreach (var prName in properties)
                {
                    for (int i = 0; i < properties.Count; i++)
                        typeOfT.GetProperty(properties[i]).SetValue(item, values[i], null);
                }
            }

            _context.SaveChanges();

            return new DataResult(true, "");
        }

        public DataResult UpdateBulk(Expression<Func<T, bool>> predicate, int pageNumber, int pageCount, List<string> properties, List<object> values)
        {
            var list = _context.Set<T>().OrderBy("Id").Where(predicate).Skip((pageNumber - 1) * pageCount).Take(pageCount);
            var typeOfT = typeof(T);
            foreach (var item in list)
            {
                for (int i = 0; i < properties.Count; i++)
                    typeOfT.GetProperty(properties[i]).SetValue(item, values[i], null);
            }

            _context.SaveChanges();

            return new DataResult(true, "");
        }

        /// <summary>
        /// Delete Entity
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public DataResult Delete(T t)
        {
            return DeleteByKey(t.Id);
        }

        /// <summary>
        /// Delete Entity by Key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataResult DeleteByKey(int id)
        {
            try
            {
                T aModel = _context.Set<T>().Where(x => x.Id == id).FirstOrDefault();

                if (aModel == null)
                    return new DataResult(false, "Silinecek kayıt bulunamıyor");

                BeforeDelete();
                _context.Set<T>().Remove(aModel);
                AfterDelete();
                _context.SaveChanges();

                return new DataResult(true, "");
            }
            catch (Exception exc)
            {
                return new DataResult(false, exc.Message +
                    exc.InnerException == null ? "" : "(" + exc.InnerException + ")"
                );
            }
        }

        public DataResult DeleteBulk(Expression<Func<T, bool>> predicate)
        {
            var list = _context.Set<T>().Where(predicate);
            foreach (var item in list)
            {
                _context.Set<T>().Remove(item);
            }

            _context.SaveChanges();

            return new DataResult(true, "");
        }

        /// <summary>
        /// Get Entity By Key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetByKey(int id)
        {
            try
            {
                T aModel = _context.Set<T>().Where(x => x.Id == id).FirstOrDefault();
                return aModel;
            }
            catch(Exception exc)
            {
                return null;
            }
        }

        /// <summary>
        /// Get All Entities
        /// </summary>
        /// <returns></returns>
        public List<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        /// <summary>
        /// Get All Entities
        /// </summary>
        /// <param name="orderBy">property name to order</param>
        /// <returns></returns>
        public List<T> GetAll(string orderBy, bool isDesc = false)
        {
            return isDesc
                ? _context.Set<T>().OrderByDescending(orderBy).ToList()
                : _context.Set<T>().OrderBy(orderBy).ToList();
        }
        
        /// <summary>
        /// Find an Entity
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public List<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return _context.Set<T>()
                    .Where(predicate)
                    .ToList();
            }
            catch(Exception exc)
            {
                return null;
            }
        }

        /// <summary>
        /// Find an Entity
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="orderBy">order by property name</param>
        /// <returns></returns>
        public List<T> GetBy(Expression<Func<T, bool>> predicate, string orderBy, bool isDesc = false)
        {
            try
            {
                return isDesc
                    ? _context.Set<T>().Where(predicate).OrderByDescending(orderBy).ToList()
                    : _context.Set<T>().Where(predicate).OrderBy(orderBy).ToList();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Entities by Page
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        public List<T> GetByPage(int pageNumber, int pageCount, string orderBy = "Id", bool isDesc = false)
        {
            try
            {
                return isDesc
                    ? _context.Set<T>().OrderByDescending(orderBy).Skip((pageNumber - 1) * pageCount).Take(pageCount).ToList()
                    : _context.Set<T>().OrderBy(orderBy).Skip((pageNumber - 1) * pageCount).Take(pageCount).ToList();
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc.Message);
                if (exc.InnerException != null)
                {
                    Console.WriteLine(exc.InnerException.Message);
                    if (exc.InnerException.InnerException != null)
                        Console.WriteLine(exc.InnerException.InnerException.Message);
                }


                return null;
            }
        }

        /// <summary>
        /// Find and Get Entities by Page
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        public List<T> GetByPage(Expression<Func<T, bool>> predicate, int pageNumber, int pageCount, string orderBy = "Id", bool isDesc = false)
        {
            try
            {
                return isDesc
                   ? _context.Set<T>().OrderByDescending(orderBy).Where(predicate).Skip((pageNumber - 1) * pageCount).Take(pageCount).ToList()
                   : _context.Set<T>().OrderBy(orderBy).Where(predicate).Skip((pageNumber - 1) * pageCount).Take(pageCount).ToList();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Count of Entities
        /// </summary>
        /// <returns></returns>
        public int GetCount()
        {
            return _context.Set<T>().Count();
        }

        /// <summary>
        /// Find and Get Count of Entities
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public int GetCount(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>()
                .Where(predicate)
                .Count();
        }

        /// <summary>
        /// Get random records 
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        public List<T> GetRandom(int limit)
        {
            if (limit <= 0)
                return new List<T>();
            
            return _context.Set<T>().OrderBy(x => Guid.NewGuid()).Take(limit).ToList();
        }

        #endregion
    }
}
