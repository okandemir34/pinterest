﻿using mySqlEntity = MySql.Data.EntityFramework;

namespace SomePinte.Data.MySql.Core
{
    using Model;
    using System.Data.Entity;

    [DbConfigurationType(typeof(mySqlEntity.MySqlEFConfiguration))]
    public class DataContext : DbContext
    {
        public DataContext() : base("MyDbContextConnectionString")
        {
        }

        public DbSet<UserInfo> UserInfos { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Cookie> Cookies { get; set; }
        public DbSet<WhiteListDomain> WhiteListDomains { get; set; }
        public DbSet<PossibleUser> PossibleUsers { get; set; }
        public DbSet<BanUser> BanUsers { get; set; }
        public DbSet<BanWord> BanWords { get; set; }
        public DbSet<BanMedia> BanMedias { get; set; }
        public DbSet<CountryUser> CountryUsers { get; set; }
        public DbSet<Location> Locations { get; set; }
       
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserInfo>().ToTable("sp_userinfos");
            modelBuilder.Entity<Message>().ToTable("sp_messages");
            modelBuilder.Entity<Cookie>().ToTable("sp_cookies");
            modelBuilder.Entity<PossibleUser>().ToTable("sp_possibleusers");
            modelBuilder.Entity<WhiteListDomain>().ToTable("sp_whitelistdomains");
            modelBuilder.Entity<BanUser>().ToTable("sp_banusers");
            modelBuilder.Entity<BanWord>().ToTable("sp_banwords");
            modelBuilder.Entity<BanMedia>().ToTable("sp_banmedias");
            modelBuilder.Entity<CountryUser>().ToTable("sp_countryusers");
            modelBuilder.Entity<Location>().ToTable("sp_locations");
           
            base.OnModelCreating(modelBuilder);
        }
    }
}