﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Pinterest.Html.Entities
{
    public class PinDetail
    {
        public string domain { get; set; }
        public string grid_description { get; set; }

        [JsonProperty("rich_summary")]
        public rich_summary richSummary { get; set; }
        public class rich_summary
        {
            public string display_name { get; set; }
            public string url { get; set; }
            public string type_name { get; set; }
            public string display_description { get; set; }
            public string site_name { get; set; }
            public string type { get; set; }
            public string id { get; set; }
        }

        public string id { get; set; }
        public string description_html { get; set; }
        public string privacy { get; set; }
        public int comment_count { get; set; }

        public class ImageAndSize
        {
            public string url { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public Images images { get; set; }
        public class Images
        {
            [JsonProperty("736x")]
            public ImageAndSize x736 { get; set; }

            [JsonProperty("474x")]
            public ImageAndSize x474 { get; set; }

            [JsonProperty("170x")]
            public ImageAndSize x170 { get; set; }

            [JsonProperty("236x")]
            public ImageAndSize x236 { get; set; }

            [JsonProperty("136x136")]
            public ImageAndSize x136 { get; set; }

            [JsonProperty("orig")]
            public ImageAndSize orig { get; set; }
        }

        public Board board { get; set; }
        public class Board
        {
            public bool is_collaborative { get; set; }
            public string layout { get; set; }
            public string name { get; set; }
            public string privacy { get; set; }
            public string url { get; set; }
            public bool collaborated_by_me { get; set; }

            public Owner owner { get; set; }
            public class Owner
            {
                public string id { get; set; }
            }

            public bool followed_by_me { get; set; }
            public string type { get; set; }
            public string id { get; set; }
            public string image_thumbnail_url { get; set; }
        }

        public bool is_repin { get; set; }
        public bool is_quick_promotable { get; set; }
        public string type { get; set; }
        public string method { get; set; }
        public bool is_whitelisted_for_tried_it { get; set; }
        public string grid_title { get; set; }
        public string image_signature { get; set; }
        public string description { get; set; }
        public bool is_playable { get; set; }
        public string link { get; set; }

        public Pinner pinner { get; set; }
        public class Pinner
        {
            public string username { get; set; }
            public string type { get; set; }
            public bool explicitly_followed_by_me { get; set; }
            public string image_xlarge_url { get; set; }
            public string full_name { get; set; }
            public string image_small_url { get; set; }
            public bool blocked_by_me { get; set; }
            public string id { get; set; }
            public string image_large_url { get; set; }
        }

        public bool is_native { get; set; }
        public DateTime created_at { get; set; }
        public int repin_count { get; set; }
        public string dominant_color { get; set; }
        public string title { get; set; }
        public bool is_video { get; set; }
    }
}
