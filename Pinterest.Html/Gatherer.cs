﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pinterest.Dto;
using Pinterest.Html.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Pinterest.Html
{
    public class Gatherer
    {
        public ResponseDto<TagResponseDto> GetMediasByTag(string tag, string bookmark = "")
        {
            string html = "";
            using (WebClient client = new WebClient())
            {
                string tagSeperated = tag.Replace(" ", "%2520");
                tag = Uri.EscapeUriString(tag);

                string nextPageUrl = "https://www.pinterest.fr/resource/BaseSearchResource/get/?source_url=%2Fsearch%2Fpins%2F%3Fq%3D{0}%26" +
                    "rs%3Dtyped&data=%7B%22options%22%3A%7B%22isPrefetch%22%3Afalse%2C%22query%22%3A%22{1}%22%2C%22scope%22%3A%22pins" +
                    "%22%2C%22bookmarks%22%3A%5B%22{2}%22%5D%7D%2C%22context%22%3A%7B%7D%7D";
                string direkUrl = "https://www.pinterest.fr/resource/BaseSearchResource/get/?source_url=%2Fsearch%2Fpins%2F%3Fq%3D{0}%26" +
                    "rs%3Dtyped&data=%7B%22options%22%3A%7B%22isPrefetch%22%3Afalse%2C%22query%22%3A%22{1}%22%2C%22scope%22%3A%22pins" +
                    "%22%7D%2C%22context%22%3A%7B%7D%7D";

                string url = "";

                if (string.IsNullOrEmpty(bookmark))
                {
                    url = string.Format(direkUrl, tagSeperated, tag);
                }
                else
                {
                    url = string.Format(nextPageUrl, tagSeperated, tag, bookmark);
                }

                client.Headers.Add("Accept", "application/json, text/javascript, */*, q=0.01");
                client.Headers.Add("Host", "www.pinterest.fr");
                client.Headers.Add("Method", "GET");
                client.Headers.Add("Referer", "https://www.pinterest.fr/");
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                client.Headers.Add("Accept-Language", "en-US,en;q=0.9");
                client.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36");
                client.Headers.Add("X-Pinterest-AppState", "active");
                client.Headers.Add("X-Requested-With", "XMLHttpRequest");
                client.Headers.Add("Cookie", "_pinterest_cm=TWc9PSYwbE9pUkcyb3NlTmRtZVl4bE13SnFRVi9OeFJDdmFiSTIyamlKWnBZeGh1T0lRQWd3OEpRVXVTRW9lYUExbGQyVEFXc2xGMzJaZFhCWTBzS0hQR0IzN2d2ZGZEdlhESmU5OXJTRGhodU1vMnF3R05UeXhwVEJZbWhWZkFJa1FidCZROXUvNnBONzFHNEV2S3orMEc4S21BYmFWQ1k9; _auth=0; _pinterest_sess=TWc9PSZWUG0rUXJpcmdFeUJFWmlKdjF4MG8zOG16M05ObXJnWHNpN1ZBamRYVmsvR3AvS2kxa2U3WEdLRWNXQ2VjWEVoTEJma1Z0ZnB6NWhHRVh1Skg0Und3OEZvT2lDTjllOFptY0p0R2UxOWdERndMM3FwTUoyK1BvM3dISDNZNzVVTXVZRUhrT21WYlVab25wdGpMNTZ0M2c9PSZPMURpaVRxN1A1SzBYTkJHMkZoMjRjUWl0Y2s9; csrftoken=k1egmHRzWffFG3uD4dQZwBxmL4YK5gDV; sessionFunnelEventLogged=1; G_ENABLED_IDPS=google; _fbp=fb.1.1540882591722.695927538");
                client.Headers.Add("X-APP-VERSION", "7fe1070");

                client.Encoding = System.Text.Encoding.UTF8;

                try
                {
                    using (Stream data = client.OpenRead(url))
                    using (Stream csStream = new GZipStream(data, CompressionMode.Decompress))
                    using (var reader = new StreamReader(csStream, Encoding.UTF8))
                    {
                        html = reader.ReadToEnd();
                    }
                }
                catch
                {
                    return new ResponseDto<TagResponseDto>(new TagResponseDto()
                    {
                        Bookmark = "",
                        Pins = new List<PinDto>(),
                        Tag = tag,
                        Terms = new List<TermDto>()
                    }, false, "401");
                }
            }

            var json = JObject.Parse(html);
            var jArr = JArray.Parse(json["resource_response"]["data"]["results"].ToString());
            var list = new List<PinDetail>();
            var bookmarkx = "";
            try
            {
                var bookmarkArr = JArray.Parse(json["resource"]["options"]["bookmarks"].ToString());
                bookmarkx = bookmarkArr[0].ToString();
            }
            catch {

            }

            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            foreach (var item in jArr)
            {
                if (item["type"].ToString() == "story")
                    continue;

                var y = JsonConvert.DeserializeObject<PinDetail>(item.ToString(), jsonSettings);
                list.Add(y);
            }

            var pinsDto = list.Where(x=>x.created_at.Year != 1).Select(x => new PinDto()
            {
                Category = "",
                CommentCount = x.comment_count,
                CreatedAt = x.created_at,
                Description = x.description,
                Id = x.id,
                ImageLarge = new ImageDto(x.images.x736.url, x.images.x736.width, x.images.x736.height),
                ImageMedium = new ImageDto(x.images.x474.url, x.images.x474.width, x.images.x474.height),
                ImageSquare = new ImageDto(x.images.x136.url, x.images.x136.width, x.images.x136.height),
                IsRepin = x.is_repin,
                Link = x.link,
                Owner = new UserSimpleDto()
                {
                    AvatarLarge = "",
                    AvatarMedium = "",
                    AvatarSmall = "",
                    FullName = "",
                    UserId = x.board == null || x.board.owner == null ? "" : x.board.owner.id,
                    Username = ""
                },
                RepinCount = x.repin_count,
                Title = x.title,
                UserSimple = new UserSimpleDto()
                {
                    AvatarLarge = x.pinner.image_xlarge_url,
                    AvatarMedium = x.pinner.image_large_url,
                    AvatarSmall = x.pinner.image_small_url,
                    FullName = x.pinner.full_name,
                    UserId = x.pinner.id,
                    Username = x.pinner.username.ToLowerInvariant()
                }
            }).ToList();

            //termsi
            var listTerms = new List<TermDto>();
            if (json["resource_response"]["data"]["guides"] != null 
                && !string.IsNullOrEmpty(json["resource_response"]["data"]["guides"].ToString()))
            {
                var jArrTerms = JArray.Parse(json["resource_response"]["data"]["guides"].ToString());
                foreach (var item in jArrTerms)
                {
                    var term = new TermDto()
                    {
                        Color = item["dominant_color"].ToString(),
                        Name = item["display"].ToString(),
                        Term = item["term"].ToString(),
                    };

                    listTerms.Add(term);
                }
            }

            return new ResponseDto<TagResponseDto>(new TagResponseDto()
            {
                Pins = pinsDto,
                Tag = tag,
                Terms = listTerms,
                Bookmark = bookmarkx
            }, true, "");
        }
    }
}
