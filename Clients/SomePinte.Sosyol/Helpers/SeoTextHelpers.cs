﻿using Pinterest.Dto;
using SomePinte.Sosyol.Models;

namespace SomePinte.Sosyol.Helpers
{
    public static class SeoTextHelpers
    {
        public static string AccountTitle(this UserDto userDto)
        {
            return string.IsNullOrEmpty(userDto.FullName)
                ? $"{userDto.Username}'s Pinterest Account, Images and Ideas"
                : $"{userDto.FullName} ({userDto.Username})'s Pinterest Account, Images and Ideas";
        }

        public static string AccountTitleFromUsername(this string username, string fullname)
        {
            return string.IsNullOrEmpty(fullname)
                ? $"{username}'s Pinterest Account, Images and Ideas"
                : $"{fullname} ({username})'s Pinterest Account, Images and Ideas";
        }

        public static string AccountSubTitle(this UserDto userDto)
        {
            return $"List Pinterest {userDto.Username} Images, Videos and Ideas shared on Pinterest Account. Also find {userDto.FullName}'s Pinterest Account Statistics, Engagement Rates and Analytics.";
        }

        public static string AccountDescription(this UserDto userDto)
        {
            return $"List Pinterest {userDto.Username} Images, Videos and Ideas shared on Pinterest Account. Also find {userDto.FullName}'s Pinterest Account Statistics, Engagement Rates and Analytics.";
        }

        public static string AccountAvatarTitle(this UserDto userDto)
        {
            return string.IsNullOrEmpty(userDto.FullName)
                ? $"{userDto.Username}'s Pinterest Account Avatar"
                : $"{userDto.FullName}'s Pinterest Account Avatar";
        }

        public static string AccountAvatarTitleFromUsername(this string username, string fullName)
        {
            return string.IsNullOrEmpty(fullName)
                ? $"{username}'s Pinterest Account Avatar"
                : $"{fullName}'s Pinterest Account Avatar";
        }

        public static string HashtagTitle(this string hashtag)
        {
            return $"Browse {hashtag} Images and Ideas on Pinterest";
        }

        public static string HashtagDescription(this string hashtag)
        {
            return $"Browse {hashtag} Images, Videos and Ideas recently shared on Pinterest. Also find suggestions for pinterest hashtag #{hashtag}.";
        }

        public static string MediaTitle(this MediaBoxViewModel media)
        {
            if (!string.IsNullOrEmpty(media.Title))
            {
                return media.Title.ToTrim(150);
            }
            else if (!string.IsNullOrEmpty(media.Caption.Trim()))
            {
                return media.Caption.ToTrim(150);
            }

            return $"{media.Username}'s Pinterest Image {media.Id}";
        }

        public static string MediaDescription(this MediaBoxViewModel media)
        {
            if (!string.IsNullOrEmpty(media.Caption))
            {
                return media.Caption.ToTrim(150);
            }
            else if (!string.IsNullOrEmpty(media.Title))
            {
                return media.Title.ToTrim(150);
            }

            return $"{media.Username}'s Pinterest Image {media.Id}";
        }
    }
}