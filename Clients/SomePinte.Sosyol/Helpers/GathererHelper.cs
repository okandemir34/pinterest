﻿using Pinterest.Cookie;
using SomePinte.Data.MySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace SomePinte.Sosyol.Helpers
{
    public class GathererHelper
    {
        public static Gatherer GetAGatherer()
        {
            var _cookieModel = Cookies.OrderBy(x => System.Guid.NewGuid()).FirstOrDefault();

            return new Gatherer(_cookieModel);
        }

        public static string GetAPopularTag()
        {
            var tagList = new List<string>() {
                "fail",
                "quotes",
                "success",
                "ideas",
                "recipe",
                "wedding",
                "project",
                "addict",
                "nails",
                "marketing",
                "tips",
                "start",
                "style",
                "bride",
                "aesthetic",
                "fashion"
            };

            return tagList.OrderBy(x => Guid.NewGuid()).FirstOrDefault();
        }

        private static string Cookies_CacheKey = "Cookies_CacheKey";
        public static List<CookieModel> Cookies
        {
            get
            {
                if (HttpContext.Current.Cache[Cookies_CacheKey] == null)
                {
                    var model = new CookieData().GetAll();

                    var cookies = model.Select(x => new CookieModel()
                    {
                        UserAgent = x.UserAgent,
                        Authorization = x.Token
                    }).ToList();

                    HttpContext.Current.Cache.Add(
                       Cookies_CacheKey
                     , cookies
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache[Cookies_CacheKey] as List<CookieModel>;
            }
        }

        public static bool ClearCookieCache()
        {
            HttpContext.Current.Cache.Remove("Cookies_CacheKey");

            return true;
        }
    }
}