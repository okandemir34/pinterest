﻿using Pinterest.Cookie;
using Pinterest.Dto;
using SomePinte.Data.MySql;
using SomePinte.Sosyol.Helpers;
using SomePinte.Sosyol.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SomePinte.Sosyol.Controllers
{
    public class AccountController : Controller
    {
        Gatherer _gatherer;

        public AccountController()
        {
            _gatherer = GathererHelper.GetAGatherer();
        }

        public ActionResult Index(string username, string userId)
        {
            var banUser = CacheHelper.BanUsers.FirstOrDefault(x => x.UserName == username || x.UserId == userId);
            if (banUser != null && banUser.Type == 2)
                return RedirectToAction("Index", "Home", new { t = "user_cant_shown" });

            var info = _gatherer.GetUserInfo(userId);
            if (!info.IsSucceed)
                return RedirectToAction("Index", new { userId = userId, t = "user_cant_found" });

            var pins = _gatherer.GetPinsByUserId(userId);
            var related = pins.Data.Where(x => x.Owner != null && x.Owner.Username != username).Select(x => x.Owner).ToList();

            var logs = StatsHelper.GetLogs(username);

            var viewModel = new UserPinsViewModel()
            {
                Medias = pins.Data.Select(x => MediaBoxViewModel.FromPinAndUserDto(x, info.Data)).ToList(),
                User = info.Data,
                RelatedUsers = related,
                Logs = logs
            };

            //popular user olarak kaydedelim
            if (info.Data.FollowerCount > 500000)
            {
                var _data = new Data.MySql.UserInfoData();

                var userInfo = _data.GetBy(x => x.Username == info.Data.Username).FirstOrDefault();
                if (userInfo == null)
                {
                    userInfo = new Model.UserInfo()
                    {
                        About = info.Data.About,
                        Avatar = info.Data.Avatar,
                        Boards = info.Data.BoardCount,
                        Followers = info.Data.FollowerCount,
                        Followings = info.Data.FollowingCount,
                        FullName = info.Data.FullName,
                        Pins = info.Data.PinCount,
                        UserId = info.Data.Id,
                        Username = info.Data.Username
                    };

                    var dbResult = _data.Insert(userInfo);
                }

            }

            if (info.IsSucceed && pins.IsSucceed && pins.Data.Count > 15)
            {
                var jsonUserInfoStr = new JavaScriptSerializer().Serialize(info.Data);
                var jsonMediasStr = new JavaScriptSerializer().Serialize(pins.Data);

                try
                {
                    System.IO.File.WriteAllText(Server.MapPath("~/_logs/SocialMany.Bot.SosyolLogImporter/user/" + info.Data.Username + ".txt"), jsonUserInfoStr);
                    System.IO.File.WriteAllText(Server.MapPath("~/_logs/SocialMany.Bot.SosyolLogImporter/media/" + info.Data.Username + ".txt"), jsonMediasStr);

                }
                catch (Exception exc)
                {
                }
            }

            

            return View(viewModel);
        }

        public ActionResult Media(string id)
        {
            var banMedia = CacheHelper.BanMedias.FirstOrDefault(x => x.MediaId == id);
            if (banMedia != null && banMedia.Type == 2)
                return RedirectToAction("Index", "Home", new { t = "media_cant_shown" });

            var pin = _gatherer.GetPinById(id);
            if (!pin.IsSucceed)
                return RedirectToAction("Index", "Home", new { t = "media_not_found" });

            var banUser = CacheHelper.BanUsers.FirstOrDefault(x => x.UserId == pin.Data.UserSimple.UserId);
            if (banUser != null && banUser.Type == 2)
                return RedirectToAction("Index", "Home", new { t = "user_cant_shown" });

            var info = _gatherer.GetUserInfo(pin.Data.UserSimple.UserId);
            if (!info.IsSucceed)
                return RedirectToAction("Index", "Home", new { t = "user_not_found" });

            var relateds = _gatherer.GetRelatedPins(id).Data.Select(
                x => MediaBoxViewModel.FromPinAndUserDto(x, x.UserSimple)).ToList();

            var viewModel = new PinViewModel()
            {
                Media = MediaBoxViewModel.FromPinAndUserDto(pin.Data, info.Data),
                RelatedMedias = relateds,
                User = info.Data
            };

            if (!string.IsNullOrEmpty(viewModel.Media.Link))
            {
                var _whiteListDomainData = new WhiteListDomainData();
                Uri uri = new Uri(viewModel.Media.Link);

                string host = uri.Host.Replace("www.", "");

                var domainInDb = _whiteListDomainData.GetBy(x => x.Domain == host).FirstOrDefault();
                if (domainInDb == null)
                    viewModel.Media.Link = "";
            }
            

            return View(viewModel);
        }

        public ActionResult Compare()
        {
            return View();
        }

        public ActionResult Top15Accounts()
        {
            return RedirectToAction("TopAccounts");
        }

        public ActionResult TopAccounts()
        {
            return View();
        }

        public ActionResult List(string slug)
        {
            var tattoos = new List<UserDto>() {
                    new UserDto("177962760183742492","maryannrizzo","Maryann Rizzo","9M")
            };

            var viewModel = new ListAccountsViewModel()
            {
                Name = slug,
                Slug = slug
            };

            switch (slug)
            {
                case "tattoos": viewModel.Users = tattoos; break;
                default: viewModel.Users = new List<UserDto>(); break;
            };

            return View(viewModel);
        }

        public ActionResult BanAccount(string username, string userid, int type)
        {
            BanUserData _banUserData = new BanUserData();
            var banUserInDb = _banUserData.GetBy(x => x.UserId == userid && x.UserName == username).FirstOrDefault();

            //yeni ekle
            if (banUserInDb == null) {
                banUserInDb = new Model.BanUser()
                {
                    CreateDate = DateTime.Now,
                    Description = "kullanici adres cubugundan ekledi",
                    Type = type,
                    UserId = userid,
                    UserName = username
                };

                var dbResultInsertResult = _banUserData.Insert(banUserInDb);

                return Content("Insert: " + dbResultInsertResult.IsSucceed.ToString());
            }

            //tip degisecek
            if (banUserInDb.Type != type) {
                banUserInDb.Type = type;
                var dbResultUpdateResult = _banUserData.Update(banUserInDb);

                return Content("Update: " + dbResultUpdateResult.IsSucceed.ToString());
            }

            return Content("Already");
        }

        public ActionResult BanMedia(string id, int type)
        {
            BanMediaData _banMediaData = new BanMediaData();
            var banMediaInDb = _banMediaData.GetBy(x => x.MediaId == id).FirstOrDefault();

            //yeni ekle
            if (banMediaInDb == null)
            {
                banMediaInDb = new Model.BanMedia()
                {
                    CreateDate = DateTime.Now,
                    Description = "kullanici adres cubugundan ekledi",
                    Type = type,
                    MediaId = id
                };

                var dbResultInsertResult = _banMediaData.Insert(banMediaInDb);

                return Content("Insert: " + dbResultInsertResult.IsSucceed.ToString());
            }

            //tip degisecek
            if (banMediaInDb.Type != type)
            {
                banMediaInDb.Type = type;
                var dbResultUpdateResult = _banMediaData.Update(banMediaInDb);

                return Content("Update: " + dbResultUpdateResult.IsSucceed.ToString());
            }

            return Content("Already");
        }
    }
}