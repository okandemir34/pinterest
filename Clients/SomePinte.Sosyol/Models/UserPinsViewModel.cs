﻿using Pinterest.Dto;
using System.Collections.Generic;
using static SomePinte.Sosyol.Helpers.SocialManyApiResponse;

namespace SomePinte.Sosyol.Models
{
    public class UserPinsViewModel
    {
        public List<MediaBoxViewModel> Medias { get; set; }
        public List<UserSimpleDto> RelatedUsers { get; set; }
        public UserDto User { get; set; }
        public List<LogItem> Logs { get; set; }
    }
}