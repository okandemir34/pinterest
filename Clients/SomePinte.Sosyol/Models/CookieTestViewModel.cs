﻿namespace SomePinte.Sosyol.Models
{
    public class CookieTestViewModel
    {
        public string AccessToken { get; set; }
        public bool IsSucceed { get; set; }
        public string Message { get; set; }
    }
}