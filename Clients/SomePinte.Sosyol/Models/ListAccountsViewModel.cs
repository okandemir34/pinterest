﻿using Pinterest.Dto;
using System.Collections.Generic;

namespace SomePinte.Sosyol.Models
{
    public class ListAccountsViewModel
    {
        public List<UserDto> Users { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
    }
}