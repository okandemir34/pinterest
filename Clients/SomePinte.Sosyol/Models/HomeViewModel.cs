﻿using System.Collections.Generic;

namespace SomePinte.Sosyol.Models
{
    public class HomeViewModel
    {
        public List<MediaBoxViewModel> RecentMedias { get; set; }
    }
}