﻿using Pinterest.Dto;
using System.Collections.Generic;

namespace SomePinte.Sosyol.Models
{
    public class PinViewModel
    {
        public MediaBoxViewModel Media { get; set; }
        public List<MediaBoxViewModel> RelatedMedias { get; set; }
        public UserDto User { get; set; }
        public int AlexaWorldRank { get; set; }
    }
}