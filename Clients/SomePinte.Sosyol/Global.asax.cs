﻿using SomePinte.Sosyol.Helpers;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SomePinte.Sosyol
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.IsLocal)
                HttpHelper.EnsureSslAndDomain(HttpContext.Current, true, true);
        }
    }
}
