﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SomePinte.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "ClearCookies", url: "cache/cookies", defaults: new { controller = "Home", action = "ClearCookies" });
            routes.MapRoute(name: "ClearBans", url: "cache/bans", defaults: new { controller = "Home", action = "ClearBans" });
            routes.MapRoute(name: "ClearPopulars", url: "cache/popular", defaults: new { controller = "Home", action = "ClearPopulars" });
            routes.MapRoute(name: "TokenTest", url: "t-test", defaults: new { controller = "Home", action = "TokenTest" });

            routes.MapRoute(name: "Login", url: "login", defaults: new { controller = "Profile", action = "Login" });
            routes.MapRoute(name: "Signup", url: "signup", defaults: new { controller = "Profile", action = "Signup" });

            routes.MapRoute(name: "Company", url: "company", defaults: new { controller = "Home", action = "Company" });
            routes.MapRoute(name: "Team", url: "team", defaults: new { controller = "Home", action = "Team" });
            routes.MapRoute(name: "Privacy", url: "privacy-policy", defaults: new { controller = "Home", action = "Privacy" });
            routes.MapRoute(name: "ReportGuide", url: "report-guide", defaults: new { controller = "Home", action = "ReportGuide" });

            routes.MapRoute(name: "SearchIndex", url: "search/", defaults: new { controller = "Home", action = "SearchIndex" });
            routes.MapRoute(name: "SearchQuery", url: "search/{query}", defaults: new { controller = "Home", action = "SearchQuery" });

            routes.MapRoute(name: "Compare", url: "account/compare", defaults: new { controller = "Account", action = "Compare" });
            routes.MapRoute(name: "TopAccount", url: "account/top-accounts", defaults: new { controller = "Account", action = "TopAccounts" });
            routes.MapRoute(name: "Top15Account", url: "account/top-15-accounts", defaults: new { controller = "Account", action = "Top15Accounts" });
            routes.MapRoute(name: "AccountBan", url: "account/{username}/{userid}/ban/{type}", defaults: new { controller = "Account", action = "BanAccount" });
            routes.MapRoute(name: "Account", url: "account/{username}/{userid}", defaults: new { controller = "Account", action = "Index" });
            routes.MapRoute(name: "MediaBan", url: "image/{id}/ban/{type}", defaults: new { controller = "Account", action = "BanMedia" });
            routes.MapRoute(name: "Media", url: "image/{id}", defaults: new { controller = "Account", action = "Media" });
            routes.MapRoute(name: "AllUsersInCountry", url: "country/all", defaults: new { controller = "Country", action = "All" });
            routes.MapRoute(name: "UserCountry", url: "country/{slug}", defaults: new { controller = "Country", action = "Index" });

            routes.MapRoute(name: "GetNextPins", url: "loadmore", defaults: new { controller = "Hashtag", action = "GetNextPins" });
            routes.MapRoute(name: "Top100Hashtag", url: "tag/top-100-hashtags", defaults: new { controller = "Hashtag", action = "Top100Hashtags" });
            routes.MapRoute(name: "Top30Category", url: "tag/top-30-categories", defaults: new { controller = "Hashtag", action = "Top30Categories" });
            routes.MapRoute(name: "HashtagBan", url: "tag/{tag}/ban/{type}", defaults: new { controller = "Hashtag", action = "BanTag" });
            routes.MapRoute(name: "Hashtag", url: "tag/{tag}", defaults: new { controller = "Hashtag", action = "Index" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
