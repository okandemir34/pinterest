﻿namespace SomePinte.WebUI.Models
{
    using SomePinte.Model;
    using System.Collections.Generic;

    public class HomeViewModel
    {
        public List<MediaBoxViewModel> RecentMedias { get; set; }
        public List<UserInfo> PopularAccounts { get; set; }
    }
}