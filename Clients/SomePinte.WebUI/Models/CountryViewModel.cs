﻿using Pinterest.Dto;
using SomePinte.Model;
using System.Collections.Generic;

namespace SomePinte.WebUI.Models
{
    public class CountryViewModel
    {
        public List<CountryUser> Users { get; set; }
        public Location Location { get; set; }
    }
}