﻿using Pinterest.Dto;
using System.Collections.Generic;
using static SomePinte.WebUI.Helpers.SocialManyApiResponse;

namespace SomePinte.WebUI.Models
{
    public class UserPinsViewModel
    {
        public List<MediaBoxViewModel> Medias { get; set; }
        public List<UserSimpleDto> RelatedUsers { get; set; }
        public UserDto User { get; set; }
        public List<LogItem> Logs { get; set; }
        public string PopularPath { get; set; }
        public int PopularRepin { get; set; }
        public int PopularCommment { get; set; }
        public string Banner { get; set; }
        public int GlobalRank { get; set; }
        public int LocationRank { get; set; }
    }
}