﻿using SomePinte.WebUI.Helpers;
using System;
using System.Collections.Generic;

namespace SomePinte.WebUI.Models
{
    public class MediaBoxViewModel
    {
        public MediaBoxViewModel()
        {
            TagsInPage = new List<string>();
            HasUserLink = true;
        }

        public MediaBoxViewModel(string userId, string userName, string fullName ,string title)
        {
            UserId = userId;
            Username = userName;
            FullName = fullName;
            Title = title;
            TagsInPage = new List<string>();
            HasUserLink = true;
            
        }

        public int Count { get; set; }

        public string Avatar { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string ImagePath { get; set; }
        public string Title { get; set; }
        public string Caption { get; set; }
        public string TimeAgo { get; set; }
        public int RepinCount { get; set; }
        public int CommentCount { get; set; }
        public bool HasUserLink { get; set; }
        public string Link { get; set; }
        public string Id { get; set; }
        public int Index { get; set; } //arayuzde sonraki icin kullaniliyor
        public bool IsRepin { get; set; }
        public string Category { get; set; }
        public DateTime Created { get; set; }

        //bu ikisi mediabox'taki tagler icin kullanilior
        public List<string> TagsInPage { get; set; }
        public string Tag { get; set; }

        public static MediaBoxViewModel FromPinAndUserDto(Pinterest.Dto.PinDto pin, Pinterest.Dto.UserDto user, bool hasUserLink = false)
        {
            return new MediaBoxViewModel()
            {
                Avatar = user.Avatar,
                Caption = pin.Description,
                FullName = user.FullName,
                ImagePath = pin.ImageLarge.Path,
                RepinCount = pin.RepinCount,
                TimeAgo = pin.CreatedAt.ToTimeAgo(),
                UserId = user.Id,
                Username = user.Username,
                CommentCount = pin.CommentCount,
                HasUserLink = hasUserLink,
                Title = pin.Title,
                Id = pin.Id,
                Link = pin.Link,
                IsRepin = pin.IsRepin,
                Category = pin.Category,
                Created = pin.CreatedAt
            };
        }

        public static MediaBoxViewModel FromPinAndUserDto(Pinterest.Dto.PinDto pin, Pinterest.Dto.UserSimpleDto user)
        {
            return new MediaBoxViewModel()
            {
                Avatar = user.AvatarMedium,
                Caption = pin.Description,
                FullName = user.FullName,
                ImagePath = pin.ImageLarge.Path,
                RepinCount = pin.RepinCount,
                TimeAgo = pin.CreatedAt.ToTimeAgo(),
                UserId = user.UserId,
                Username = user.Username,
                CommentCount = pin.CommentCount,
                HasUserLink = true,
                Title = pin.Title,
                Id = pin.Id,
                Link = pin.Link,
                IsRepin = pin.IsRepin,
                Category = pin.Category,
                Created = pin.CreatedAt
            };
        }
    }
}