﻿using System.Collections.Generic;

namespace SomePinte.WebUI.Models
{
    public class TagPinsViewModel
    {
        public List<MediaBoxViewModel> Medias { get; set; }
        public List<string> Relateds { get; set; }
        public string Tag { get; set; }
        public string Bookmark { get; set; }
        public bool FromUser { get; set; }
    }
}