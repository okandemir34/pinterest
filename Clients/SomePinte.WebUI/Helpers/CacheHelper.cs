﻿using SomePinte.Data.MySql;
using SomePinte.Model;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

namespace SomePinte.WebUI.Helpers
{
    public static class CacheHelper
    {
        private static string PopularUsers_CacheKey = "PopularUsers_CacheKey";
        public static List<UserInfo> PopularUsers
        {
            get
            {
                if (HttpContext.Current.Cache[PopularUsers_CacheKey] == null)
                {
                    var model = new UserInfoData().GetAll("Followers", true);

                    HttpContext.Current.Cache.Add(
                       PopularUsers_CacheKey
                     , model
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache[PopularUsers_CacheKey] as List<UserInfo>;
            }
        }

        public static bool ClearPopularUsersCache()
        {
            HttpContext.Current.Cache.Remove("PopularUsers_CacheKey");

            return true;
        }

        private static string BanUsers_CacheKey = "BanUsers_CacheKey";
        public static List<BanUser> BanUsers
        {
            get
            {
                if (HttpContext.Current.Cache[BanUsers_CacheKey] == null)
                {
                    var model = new BanUserData().GetAll();

                    HttpContext.Current.Cache.Add(
                       BanUsers_CacheKey
                     , model
                     , null
                     , DateTime.Now.AddDays(7)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache[BanUsers_CacheKey] as List<BanUser>;
            }
        }

        private static string BanWords_CacheKey = "BanWords_CacheKey";
        public static List<BanWord> BanWords
        {
            get
            {
                if (HttpContext.Current.Cache[BanWords_CacheKey] == null)
                {
                    var model = new BanWordData().GetAll();

                    HttpContext.Current.Cache.Add(
                       BanWords_CacheKey
                     , model
                     , null
                     , DateTime.Now.AddDays(7)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache[BanWords_CacheKey] as List<BanWord>;
            }
        }

        private static string BanMedias_CacheKey = "BanMedias_CacheKey";
        public static List<BanMedia> BanMedias
        {
            get
            {
                if (HttpContext.Current.Cache[BanMedias_CacheKey] == null)
                {
                    var model = new BanMediaData().GetAll();

                    HttpContext.Current.Cache.Add(
                       BanMedias_CacheKey
                     , model
                     , null
                     , DateTime.Now.AddDays(7)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache[BanMedias_CacheKey] as List<BanMedia>;
            }
        }

        public static bool ClearBansCache()
        {
            HttpContext.Current.Cache.Remove(BanUsers_CacheKey);
            HttpContext.Current.Cache.Remove(BanWords_CacheKey);
            HttpContext.Current.Cache.Remove(BanMedias_CacheKey);

            return true;
        }
    }
}