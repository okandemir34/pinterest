﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using static SomePinte.WebUI.Helpers.SocialManyApiResponse;

namespace SomePinte.WebUI.Helpers
{
    /// <summary>
    /// socailmany api'den bilgileri alir ve ekrana basar
    /// </summary>
    public class StatsHelper
    {
        public static List<LogItem> GetLogs(string username, DateTime date, int followers, int followings, int pins)
        {
            try
            {
                using (var client = new WebClient())
                {
                    var json = client.DownloadString("https://socialmany.com/pinterest/api/" + username);
                    var apiResponse = JsonConvert.DeserializeObject<SocialManyApiResponse>(json);

                    return apiResponse.Logs;
                }
            }
            catch(Exception ex) {
                return new List<LogItem>();
            }
        }
    }

    public class SocialManyApiResponse
    {
        public bool IsSucceed { get; set; }
        public int Status { get; set; }
        public List<LogItem> Logs { get; set; }

        public class LogItem
        {
            public DateTime Date { get; set; }
            public int Followers { get; set; }
            public int Followings { get; set; }
            public int Pins { get; set; }
        }
    }
}