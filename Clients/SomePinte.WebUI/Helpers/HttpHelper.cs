﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SomePinte.WebUI.Helpers
{
    public static class HttpHelper
    {
        public static bool IsTablet()
        {
            var userAgent = HttpContext.Current.Request.UserAgent;

            if (userAgent == null)
                return false;

            Regex r = new Regex("Tablet|iPad|PlayBook|BB10|Z30|Nexus 10|Nexus 7|GT-P|SCH-I800|Xoom|Kindle|Silk|KFAPWI", RegexOptions.IgnoreCase);
            bool isTablet = r.IsMatch(userAgent);
            return isTablet;
        }

        public static bool IsAndroid()
        {
            var userAgent = HttpContext.Current.Request.UserAgent.ToLower();

            if (userAgent == null)
                return false;

            return userAgent.Contains("android");
        }

        public static bool IsPhone()
        {
            return HttpContext.Current.Request.Browser.IsMobileDevice && !IsTablet();
        }

        public static bool IsDesktop()
        {
            return !IsPhone() && !IsTablet();
        }

        public static string Canonical(string relativePath)
        {
            var root = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

            if (relativePath == "/" || string.IsNullOrEmpty(relativePath))
                return root;

            return root.Trim('/') + "/" + relativePath.Trim('/') + "/";
        }

        public static void EnsureSslAndDomain(HttpContext context, bool redirectNonWww, bool redirectSsl)
        {
            var redirect = false;
            var uri = context.Request.Url;
            var scheme = uri.GetComponents(UriComponents.Scheme, UriFormat.Unescaped);
            var host = uri.GetComponents(UriComponents.Host, UriFormat.Unescaped);
            var pathAndQuery = uri.GetComponents(UriComponents.PathAndQuery, UriFormat.Unescaped);

            if (redirectSsl && !scheme.Equals("https"))
            {
                scheme = "https";
                redirect = true;
            }

            if (!redirectSsl && scheme.Equals("https"))
            {
                scheme = "http";
                redirect = true;
            }

            if (redirectNonWww && host.StartsWith("www", StringComparison.OrdinalIgnoreCase))
            {
                host = host.Replace("www.", "");
                redirect = true;
            }

            if (redirect)
            {
                context.Response.Status = "301 Moved Permanently";
                context.Response.StatusCode = 301;
                context.Response.AddHeader("Location", scheme + "://" + host + pathAndQuery);
            }
        }
    }
}