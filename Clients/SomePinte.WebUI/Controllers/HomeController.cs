﻿using Pinterest.Cookie;
using SomePinte.Data.MySql;
using SomePinte.WebUI.Helpers;
using SomePinte.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SomePinte.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var randomPopularTag = GathererHelper.GetAPopularTag();
            var gatherer = new Pinterest.Html.Gatherer();
            var pins = gatherer.GetMediasByTag(randomPopularTag);

            var medias = pins.Data.Pins.Select(
              x => MediaBoxViewModel.FromPinAndUserDto(x, x.UserSimple)).ToList();

            Random rnd = new Random();
            var populars = CacheHelper.PopularUsers.OrderBy(x => rnd.Next()).Take(3).ToList();
            
            var viewModel = new HomeViewModel()
            {
                RecentMedias = medias,
                PopularAccounts = populars,
            };

            return View(viewModel);
        }

        public ActionResult Company()
        {
            return View();
        }

        public ActionResult Team()
        {
            return View();
        }

        public ActionResult Privacy()
        {
            return View();
        }

        public ActionResult ReportGuide()
        {
            return View();
        }

        public ActionResult TokenTest(int id = -1, int start = 0, int count = 100)
        {
            var list = new List<CookieTestViewModel>();

            var data = new CookieData();

            var cookies = id == -1
                ? data.GetAll()
                : data.GetBy(x => x.Id == id);

            if (cookies == null)
                cookies = new List<Model.Cookie>();

            int ok = 0;
            int none = 0;
            int pasif = 0;

            foreach (var cookieInDb in cookies)
            {
                if (!cookieInDb.IsActive && cookies.Count != 1)
                {
                    pasif += 1;
                    continue;
                }

                var item = new CookieTestViewModel()
                {
                    AccessToken = cookieInDb.Token
                };

                try
                {
                    var helper = new Gatherer(new CookieModel()
                    {
                        Authorization = cookieInDb.Token,
                        ip = "",
                        password = "",
                        port = 0,
                        UserAgent = cookieInDb.UserAgent,
                        username = cookieInDb.Username
                    });

                    var mediaResponse = helper.GetPinsByUserId(new List<string>() { "177962760183742492", "21321935644262401", "26036641504165889"
                        , "2814955925536769","39265965409566721","26247747736699141","47639845966708737","54817457872831177","218143313109188609" }
                            .OrderBy(x => Guid.NewGuid()).FirstOrDefault());

                    if (mediaResponse.IsSucceed)
                    {
                        item.IsSucceed = true;
                        item.Message = "";
                        ok += 1;
                    }
                    else
                    {
                        cookieInDb.IsActive = false;
                        data.Update(cookieInDb);
                        none += 1;

                        item.IsSucceed = false;
                        item.Message = mediaResponse.Message;
                    }

                    list.Add(item);
                }
                catch (Exception exc)
                {
                    item.IsSucceed = false;
                    item.Message = exc.Message;

                    list.Add(item);
                }

                System.Threading.Thread.Sleep(new Random().Next(500, 600));
            }

            ViewBag.Ok = ok;
            ViewBag.None = none;
            ViewBag.Pasif = pasif;

            return View(list);
        }

        public ActionResult ClearCookies()
        {
            GathererHelper.ClearCookieCache();

            return RedirectToAction("Index");
        }

        public ActionResult ClearBans()
        {
            CacheHelper.ClearBansCache();

            return RedirectToAction("Index");
        }

        public ActionResult ClearPopulars()
        {
            CacheHelper.ClearPopularUsersCache();

            return RedirectToAction("Index");
        }

        public ActionResult SearchIndex()
        {
            return View();
        }

        public ActionResult SearchQuery(string query)
        {
            if (string.IsNullOrEmpty(query))
                return View();

            var gatherer = GathererHelper.GetAGatherer();
            var suggestions = gatherer.GetSuggestions(query);

            return View(suggestions.Data);
        }
    }
}