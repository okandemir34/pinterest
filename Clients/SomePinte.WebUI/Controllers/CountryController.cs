﻿using Newtonsoft.Json;
using Pinterest.Cookie;
using SomePinte.Data.MySql;
using SomePinte.WebUI.Helpers;
using SomePinte.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SomePinte.WebUI.Controllers
{
    public class CountryController : Controller
    {
        CountryUserData _countryUserData;
        LocationData _locationData;

        public CountryController()
        {
            _countryUserData = new CountryUserData();
            _locationData = new LocationData();
        }

        public ActionResult Index(string slug)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { q = "location-slug-empty" });

            var location = _locationData.GetBy(x => x.Slug == slug).FirstOrDefault();
            if (location == null)
                return RedirectToAction("Index", "Home", new { q = "location-not-found" });


            var datas = _countryUserData.GetBy(x => x.LocationId == location.Id, "Followers", true);

            var model = new CountryViewModel()
            {
                Users = datas,
                Location = location,
            };

            return View(model);
        }

        public ActionResult All()
        {
            var datas = _countryUserData.GetByPage(1,100,"Followers",true);

            var model = new CountryViewModel()
            {
                Users = datas,
            };

            return View(model);
        }

        public JsonResult NextAll(int page = 2)
        {
            var datas = _countryUserData.GetByPage(page, 100, "Followers", true);
            var result = JsonConvert.DeserializeObject<List<Model.CountryUser>>(datas.ToString());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}