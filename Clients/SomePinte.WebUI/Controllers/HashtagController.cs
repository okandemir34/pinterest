﻿using Pinterest.Cookie;
using SomePinte.Data.MySql;
using SomePinte.WebUI.Helpers;
using SomePinte.WebUI.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SomePinte.WebUI.Controllers
{
    public class HashtagController : Controller
    {
        Gatherer _gatherer;

        public HashtagController()
        {
            _gatherer = Helpers.GathererHelper.GetAGatherer();
        }

        public ActionResult Index(string tag)
        {
            if (string.IsNullOrEmpty(tag))
                return RedirectToAction("Index", "Home");

            var banWord = CacheHelper.BanWords.FirstOrDefault(x => x.Word == tag);
            if (banWord != null && banWord.Type == 2)
                return RedirectToAction("Index", "Home", new { t = "tag_cant_shown" });
            
            tag = tag.Replace("-", " ");
            var pins = new Pinterest.Html.Gatherer().GetMediasByTag(tag);
            if (!pins.IsSucceed && pins.Message == "401") {
                //carsi pazar karisti, popular birini alip ekrana verelim
                var randomPopular1 = CacheHelper.PopularUsers.OrderBy(x => Guid.NewGuid()).FirstOrDefault();
                var randomPopular2 = CacheHelper.PopularUsers.OrderBy(x => Guid.NewGuid()).FirstOrDefault();
                var userPins1 = _gatherer.GetPinsByUserId(randomPopular1.UserId);
                var userPins2 = _gatherer.GetPinsByUserId(randomPopular2.UserId);

                var userMedias1 = userPins1.Data.Select(
                        x => MediaBoxViewModel.FromPinAndUserDto(x, x.UserSimple)).ToList();
                var userMedias2 = userPins2.Data.Select(
                        x => MediaBoxViewModel.FromPinAndUserDto(x, x.UserSimple)).ToList();

                userMedias1.AddRange(userMedias2);

                var userViewModel = new TagPinsViewModel()
                {
                    Medias = userMedias1.OrderBy(x=>Guid.NewGuid()).ToList(),
                    Relateds = new System.Collections.Generic.List<string>(),
                    Tag = tag,
                    Bookmark = "",
                    FromUser = true
                };

                return View(userViewModel);
            }

            var medias = pins.Data.Pins.Select(
              x => MediaBoxViewModel.FromPinAndUserDto(x, x.UserSimple)).ToList();

            try {
                var _gatherer = GathererHelper.GetAGatherer();
                var suggestions = _gatherer.GetSuggestions(tag);

                if (suggestions.IsSucceed && suggestions.Data.Terms.Count() > 3)
                {
                    var jsonUserInfoStr = new JavaScriptSerializer().Serialize(suggestions);

                    System.IO.File
                        .WriteAllText(Server.MapPath("~/_logs/SocialMany.Bot.SosyolLogImporter/hashtag/" + tag + ".txt"),
                        jsonUserInfoStr);
                }
            }
            catch
            {

            }

            if (medias.Count == 0)
            {
                var randomPopularTag = GathererHelper.GetAPopularTag();
                pins = new Pinterest.Html.Gatherer().GetMediasByTag(randomPopularTag);

                medias = pins.Data.Pins.Select(
              x => MediaBoxViewModel.FromPinAndUserDto(x, x.UserSimple)).ToList();
            }

            var viewModel = new TagPinsViewModel()
            {
                Medias = medias,
                Relateds = pins.Data.Terms.Select(x => x.Term).ToList(),
                Tag = tag,
                Bookmark = pins.Data.Bookmark,
                FromUser = false
            };

            return View(viewModel);
        }

        public ActionResult Top100Hashtags()
        {
            return View();
        }

        public ActionResult Top30Categories()
        {
            return View();
        }

        public JsonResult GetNextPins(string tag, string bookmark)
        {
            var pins = new Pinterest.Html.Gatherer().GetMediasByTag(tag, bookmark);
            var medias = pins.Data.Pins.Select(
              x => MediaBoxViewModel.FromPinAndUserDto(x, x.UserSimple)).ToList();

            return Json(new
            {
                bookmark = pins.Data.Bookmark,
                medias = medias.Select(x => new
                {
                    avatar = x.Avatar,
                    username = x.Username,
                    userid = x.UserId,
                    fullname = x.FullName,
                    image = x.ImagePath,
                    caption = x.Caption,
                    mediaid = x.Id,
                    timeago = x.TimeAgo,
                    repincount = x.RepinCount
                })
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BanTag(string tag, int type)
        {
            BanWordData _banWordData = new BanWordData();
            var banWorkInDb = _banWordData.GetBy(x => x.Word == tag).FirstOrDefault();

            //yeni ekle
            if (banWorkInDb == null)
            {
                banWorkInDb = new Model.BanWord()
                {
                    CreateDate = DateTime.Now,
                    Description = "kullanici adres cubugundan ekledi",
                    Type = type,
                    Word = tag
                };

                var dbResultInsertResult = _banWordData.Insert(banWorkInDb);

                return Content("Insert: " + dbResultInsertResult.IsSucceed.ToString());
            }

            //tip degisecek
            if (banWorkInDb.Type != type)
            {
                banWorkInDb.Type = type;
                var dbResultUpdateResult = _banWordData.Update(banWorkInDb);

                return Content("Update: " + dbResultUpdateResult.IsSucceed.ToString());
            }

            return Content("Already");
        }
    }
}