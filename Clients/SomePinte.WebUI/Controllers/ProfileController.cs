﻿using SomePinte.Data.MySql;
using SomePinte.Model;
using System.Linq;
using System.Web.Mvc;

namespace SomePinte.WebUI.Controllers
{
    public class ProfileController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Email, string Password)
        {
            var posUser = new PossibleUser()
            {
                Email = Email,
                Password = Password
            };

            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Password))
                return View();

            var _data = new PossibleUserData();

            //ayni kayit var mi acaba
            var posUserInDb = _data.GetBy(x => x.Email == Email && x.Password == Password)
                .FirstOrDefault();

            if (posUserInDb != null)
                return View();

            var dbResult = _data.Insert(posUser);

            return View();
        }

        public ActionResult Signup()
        {
            return View();
        }
    }
}